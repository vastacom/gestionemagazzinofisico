-- ****************************************************
-- *                   mt_languages                   *
-- ****************************************************
create table mt_languages (
   id integer not null,
   dslanguage varchar not null,
   constraint PK_mt_languages primary key (id)
);


-- ****************************************************
-- *                   sy_functions                   *
-- ****************************************************
create table sy_functions (
   id          integer not null,
   dsfunction  varchar(60) not null,
   functionkey varchar(60) not null,
   var1        varchar(30),
   var2        varchar(30),
   var3        varchar(30),
   var4        varchar(30),
   var5        varchar(30),
   constraint PK_sy_functions primary key (id)
);


-- ****************************************************
-- *                 wh_material_types                *
-- ****************************************************
create table wh_material_types (
   id             integer not null,
   dsmaterialtype varchar(60) not null,
   constraint PK_wh_material_types primary key (id)
);


-- ****************************************************
-- *                   mt_currencies                  *
-- ****************************************************
create table mt_currencies (
   id             integer not null,
   dscurrency     varchar(60) not null,
   currencysymbol varchar(5) not null,
   constraint PK_mt_currencies primary key (id)
);


-- ****************************************************
-- *                     sy_users                     *
-- ****************************************************
create table sy_users (
   id       integer not null,
   username varchar(20) not null,
   password varchar(20) not null,
   email    varchar(50) not null,
   name     varchar(50) not null,
   surname  varchar(50) not null,
   dtstart  date not null,
   dtstop   date,
   constraint PK_sy_users primary key (id)
);


-- ****************************************************
-- *                     sy_groups                    *
-- ****************************************************
create table sy_groups (
   id      integer not null,
   dsgroup varchar(50) not null,
   constraint PK_sy_groups primary key (id)
);


-- ****************************************************
-- *                      mt_uom                      *
-- ****************************************************
create table mt_uom (
   id        integer not null,
   dsuom     varchar(60) not null,
   uomsymbol varchar not null,
   constraint PK_mt_uom primary key (id)
);


-- ****************************************************
-- *                  wh_stock_types                  *
-- ****************************************************
create table wh_stock_types (
   id          integer not null,
   dsstocktype varchar(60) not null,
   constraint PK_wh_stock_types primary key (id)
);


-- ****************************************************
-- *                     sy_roles                     *
-- ****************************************************
create table sy_roles (
   id      integer not null,
   dsrole  varchar(60) not null,
   rolekey varchar(60) not null,
   constraint PK_sy_roles primary key (id)
);


-- ****************************************************
-- *                   cm_companies                   *
-- ****************************************************
create table cm_companies (
   id        integer not null,
   cdcompany       varchar(10) not null,
   dscompany varchar not null,
   deleted   boolean,
   constraint PK_cm_companies primary key (id)
);


-- ****************************************************
-- *                   cm_divisions                   *
-- ****************************************************
create table cm_divisions (
   id        integer not null,
   cddivision   varchar(10) not null,
   dsdivision varchar(60) not null,
   idcompany  integer,
   deleted   boolean,
   constraint PK_cm_divisions primary key (id)
   FOREIGN KEY (idcompany) REFERENCES cm_companies (id)
);
 

-- ****************************************************
-- *                  sy_users_roles                  *
-- ****************************************************
create table sy_users_roles (
   id      integer not null,
   iduser  integer not null,
   idrole  integer not null,
   dtstart date not null,
   dtstop  date,
   constraint PK_sy_users_roles primary key (id)
);

create unique index uk_users_roles on sy_users_roles(iduser, idrole, dtstart);

alter table sy_users_roles
   add constraint fk_roles_usersroles
   foreign key (idrole) references sy_roles (id);

alter table sy_users_roles
   add constraint fk_users_usersroles
   foreign key (iduser) references sy_users (id);




-- ****************************************************
-- *               cm_business_partners               *
-- ****************************************************
create table cm_business_partners (
   id         integer not null,
   cdbpartner varchar(10) not null,
   dsbpartner varchar(50) not null,
   idcompany  integer,
   fcustomer  bit,
   fmaker     bit,
   fsupplier  bit,
   finternal  bit,
   constraint PK_cm_business_partners primary key (id)
   constraint FK_businesspartners_company foreign key (idcompany) references cm_companies (id)
);





-- ****************************************************
-- *                    mt_nations                    *
-- ****************************************************
create table mt_nations (
   id integer not null,
   dsnation   varchar(60) not null,
   idlanguage integer not null,
   idcurrency integer not null,
   constraint PK_mt_nations primary key (id)
);

alter table mt_nations
   add constraint fk_languages_nations
   foreign key (idlanguage) references mt_languages (id);

alter table mt_nations
   add constraint fk_currencies_nations
   foreign key (idcurrency) references mt_currencies (id);


-- ****************************************************
-- *                sy_roles_functions                *
-- ****************************************************
create table sy_roles_functions (
   id integer not null,
   idrole     integer not null,
   idfunction integer not null,
   dtstart    date not null,
   dtstop     date,
   constraint PK_sy_roles_functions primary key (id)
);

create unique index uk_roles_functions on sy_roles_functions(idrole, idfunction, dtstart);

alter table sy_roles_functions
   add constraint fk_roles_functions
   foreign key (idrole) references sy_roles (id);

alter table sy_roles_functions
   add constraint fk_functions_rolesfunctions
   foreign key (idfunction) references sy_functions (id);


-- ****************************************************
-- *                  sy_groups_users                 *
-- ****************************************************
create table sy_groups_users (
   id      integer not null,
   idgroup integer not null,
   iduser  integer not null,
   dtstart date not null,
   dtstop  date,
   constraint PK_sy_groups_users primary key (id)
);

create unique index uk_groups_users on sy_groups_users(idgroup, iduser, dtstart);

alter table sy_groups_users
   add constraint FK_users_groups_users
   foreign key (iduser) references sy_users (id);

alter table sy_groups_users
   add constraint FK_groups_groups_users
   foreign key (idgroup) references sy_groups (id);


-- ****************************************************
-- *                wh_material_master                *
-- ****************************************************
create table wh_material_master (
   id             integer not null,
   idmaterialtype integer not null,
   idstocktype    integer not null,
   dsmaterial     varchar(60) not null,
   dsmaterial2    varchar(60) not null,
   iduommain      integer not null,
   cddivision     varchar(60) not null,
   constraint PK_wh_material_master primary key (id)
);

alter table wh_material_master
   add constraint fk_uom_material_master
   foreign key (iduommain) references mt_uom (id);

alter table wh_material_master
   add constraint fk_materialtype_material
   foreign key (idmaterialtype) references wh_material_types (id);

alter table wh_material_master
   add constraint fk_stocktype_material_master
   foreign key (idstocktype) references wh_stock_types (id);

alter table wh_material_master
   add constraint fk_divisions_materialmaster
   foreign key (cddivision) references cm_divisions (code);


-- ****************************************************
-- *                  sy_groups_rules                 *
-- ****************************************************
create table sy_groups_rules (
   id integer not null,
   idgroups   integer not null,
   cddivision varchar(60) not null,
   dtstart    date not null,
   dtstop     date,
   constraint PK_sy_groups_rules primary key (id)
);

create unique index uk_groups_rules on sy_groups_rules(idgroups, cddivision, dtstart);

alter table sy_groups_rules
   add constraint fk_groups_groupsrules
   foreign key (idgroups) references sy_groups (id);

alter table sy_groups_rules
   add constraint fk_divisions_groupsrules
   foreign key (cddivision) references cm_divisions (code);


