--
-- File generated with SQLiteStudio v3.3.3 on dom lug 18 11:00:57 2021
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: cm_companies
CREATE TABLE cm_companies (id INTEGER NOT NULL, cdcompany varchar NOT NULL, dscompany varchar NOT NULL, deleted boolean, CONSTRAINT PK_cm_companies PRIMARY KEY (id));
INSERT INTO cm_companies (id, cdcompany, dscompany, deleted) VALUES (1, 'ONE', 'CompanyOne', 0);
INSERT INTO cm_companies (id, cdcompany, dscompany, deleted) VALUES (2, 'TWO', 'CompanyTwo', 0);

-- Table: mt_currencies
CREATE TABLE mt_currencies (id INTEGER NOT NULL CONSTRAINT PK_mt_currencies PRIMARY KEY ASC AUTOINCREMENT, cdcurrency VARCHAR (5) NOT NULL, dscurrency varchar (60) NOT NULL, currencysymbol varchar (5) NOT NULL, deleted BOOLEAN DEFAULT (0) NOT NULL);
INSERT INTO mt_currencies (id, cdcurrency, dscurrency, currencysymbol, deleted) VALUES (1, 'EUR', 'Euro', '�', 0);
INSERT INTO mt_currencies (id, cdcurrency, dscurrency, currencysymbol, deleted) VALUES (2, 'USD', 'Dollaro Statunitense', '$', 0);
INSERT INTO mt_currencies (id, cdcurrency, dscurrency, currencysymbol, deleted) VALUES (3, 'LIT', 'Lira Italiana', '�', 1);
INSERT INTO mt_currencies (id, cdcurrency, dscurrency, currencysymbol, deleted) VALUES (4, 'LST', 'Lira Sterlina', '�', 0);

-- Table: mt_languages
CREATE TABLE mt_languages (id INTEGER CONSTRAINT PK_mt_languages PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cdlanguage VARCHAR (5) NOT NULL, dslanguage VARCHAR (60) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO mt_languages (id, cdlanguage, dslanguage, deleted) VALUES (1, 'EN', 'English', 0);
INSERT INTO mt_languages (id, cdlanguage, dslanguage, deleted) VALUES (2, 'IT', 'Italiano', 0);
INSERT INTO mt_languages (id, cdlanguage, dslanguage, deleted) VALUES (3, 'FR', 'Francais', 0);
INSERT INTO mt_languages (id, cdlanguage, dslanguage, deleted) VALUES (4, 'ES', 'Espanol', 0);
INSERT INTO mt_languages (id, cdlanguage, dslanguage, deleted) VALUES (5, 'DE', 'Deutsche', 0);

-- Table: mt_nations
CREATE TABLE mt_nations (id INTEGER CONSTRAINT PK_mt_nations PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cdnation VARCHAR (5) NOT NULL, dsnation VARCHAR (60) NOT NULL, idlanguage NUMERIC CONSTRAINT fk_languages_nations REFERENCES mt_languages (idlanguage) NOT NULL, idcurrency NUMERIC CONSTRAINT fk_currencies_nations REFERENCES mt_currencies NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO mt_nations (id, cdnation, dsnation, idlanguage, idcurrency, deleted) VALUES (1, 'ITA', 'Italia', 2, 1, 0);
INSERT INTO mt_nations (id, cdnation, dsnation, idlanguage, idcurrency, deleted) VALUES (2, 'ENG', 'England', 1, 4, 0);
INSERT INTO mt_nations (id, cdnation, dsnation, idlanguage, idcurrency, deleted) VALUES (3, 'ESP', 'Espana', 4, 1, 0);
INSERT INTO mt_nations (id, cdnation, dsnation, idlanguage, idcurrency, deleted) VALUES (4, 'USA', 'United States of America', 1, 2, 0);

-- Table: mt_uom
CREATE TABLE mt_uom (id INTEGER CONSTRAINT PK_mt_uom PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cduom VARCHAR (5) NOT NULL, dsuom VARCHAR (60) NOT NULL, uomsymbol VARCHAR (5) NOT NULL, deleted BOOLEAN DEFAULT (0) NOT NULL);
INSERT INTO mt_uom (id, cduom, dsuom, uomsymbol, deleted) VALUES (1, 'KG', 'Kilogrammi', 'KG', 0);
INSERT INTO mt_uom (id, cduom, dsuom, uomsymbol, deleted) VALUES (2, 'PZ', 'Pezzi', 'PZ', 0);
INSERT INTO mt_uom (id, cduom, dsuom, uomsymbol, deleted) VALUES (3, 'M', 'Metri', 'M', 0);
INSERT INTO mt_uom (id, cduom, dsuom, uomsymbol, deleted) VALUES (4, 'L', 'Litri', 'L', 0);

-- Table: sy_functions
CREATE TABLE sy_functions (id INTEGER CONSTRAINT PK_sy_functions PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cdfunction VARCHAR (5) NOT NULL, dsfunction VARCHAR (60) NOT NULL, functionkey VARCHAR (60) NOT NULL, var1 VARCHAR (30), var2 VARCHAR (30), var3 VARCHAR (30), var4 VARCHAR (30), var5 VARCHAR (30), deleted BOOLEAN DEFAULT (0) NOT NULL);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (0, 'SYFNC', 'Funzioni', 'nSyFunctions', 'nSystem', 'FrmSyFunctions', '1', 'Sistema', '1', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (1, 'SYGRP', 'Gruppi', 'nSyGroups', 'nSystem', 'FrmSyGroups', '3', 'Sistema', '1', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (2, 'SYUSR', 'Utenti', 'nSyUsers', 'nSystem', 'FrmSyUsers', '4', 'Sistema', '1', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (3, 'MTLNG', 'Lingue', 'nMtLanguages', 'nMasterData', 'FrmMtLanguages', '1', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (4, 'MTNAT', 'Nazioni', 'nMtNations', 'nMasterData', 'FrmMtNations', '3', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (5, 'MTCUR', 'Valute', 'nMtCurrencies', 'nMasterData', 'FrmMtCurrencies', '2', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (6, 'MTUOM', 'Unit� di Misura', 'nMtUom', 'nMasterData', 'FrmMtUom', '4', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (7, 'SYROL', 'Ruoli', 'nSyRoles', 'nSystem', 'FrmSyRoles', '2', 'Sistema', '1', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (8, 'MTCOM', 'Societ�', 'nCmCompanies', 'nMasterData', 'FrmCmCompanies', '5', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (9, 'MTDIV', 'Divisioni', 'nCmDivisions', 'nMasterData', 'FrmCmDivisions', '6', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (10, 'WHMATTY', 'Tipo Materiale', 'nWhMaterialTypes', 'nCustomizing', 'FrmWhMaterialTypes', '1', 'Customizing', '2', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (11, 'WHSTOTY', 'Tipo Stock', 'nWhStockTypes', 'nCustomizing', 'FrmWhStockTypes', '2', 'Customizing', '2', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (12, 'WHWHMASTER', 'Magazzini', 'nWhWhMaster', 'nMasterData', 'FrmWhWarehouseMaster', '7', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (13, 'WHLOCTY', 'Tipo Ubicazione', 'nWhLocationTypes', 'nCustomizing', 'FrmWhLocationTypes', '3', 'Customizing', '2', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (14, 'WHLOCMA', 'Ubicazioni', 'nWhLocationMaster', 'nMasterData', 'FrmWhLocationMaster', '8', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (15, 'WHMATMA', 'Materiali', 'nWhMaterialMaster', 'nMasterData', 'FrmWhMaterialMaster', '9', 'Anagrafica', '3', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (16, 'WHSTORORD', 'Inserisci Ordine', 'nWhStorageOrders', 'nStorage', 'FrmWhStorageOrders', '1', 'Deposito', '4', 0);
INSERT INTO sy_functions (id, cdfunction, dsfunction, functionkey, var1, var2, var3, var4, var5, deleted) VALUES (17, 'WHSTOREXE', 'Esegui Deposito', 'nWhStorageExec', 'nStorage', 'FrmWhStorageExec', '2', 'Deposito', '4', 0);

-- Table: sy_groups
CREATE TABLE sy_groups (id INTEGER CONSTRAINT PK_sy_groups PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cdgroup VARCHAR (5) NOT NULL, dsgroup VARCHAR (50) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO sy_groups (id, cdgroup, dsgroup, deleted) VALUES (1, 'GEN', 'Magazzino Generale', 0);
INSERT INTO sy_groups (id, cdgroup, dsgroup, deleted) VALUES (2, 'ADM', 'Administrator', 1);
INSERT INTO sy_groups (id, cdgroup, dsgroup, deleted) VALUES (3, 'SPE', 'Spedizioni', 0);

-- Table: sy_groups_users
CREATE TABLE sy_groups_users (id INTEGER CONSTRAINT PK_sy_groups_users PRIMARY KEY ASC AUTOINCREMENT NOT NULL, idgroup INTEGER CONSTRAINT FK_groups_groups_users REFERENCES sy_groups (id) NOT NULL, iduser INTEGER CONSTRAINT FK_users_groups_users REFERENCES sy_users (id) NOT NULL, dtstart DATE NOT NULL, dtstop DATE);
INSERT INTO sy_groups_users (id, idgroup, iduser, dtstart, dtstop) VALUES (1, 1, 2, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');

-- Table: sy_roles
CREATE TABLE sy_roles (id INTEGER CONSTRAINT PK_sy_roles PRIMARY KEY ASC AUTOINCREMENT NOT NULL, cdrole VARCHAR (5) NOT NULL, dsrole VARCHAR (60) NOT NULL, rolekey VARCHAR (60) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (1, 'ADM', 'Amministratore', 'admin', 0);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (2, 'USR', 'Utente', 'User', 0);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (8, 'PRO', 'Prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (9, 'PRO', 'Prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (10, 'LOG', 'Logistico', 'logistic', 0);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (11, 'PRO', 'Prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (12, 'PRO', 'prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (13, 'PRO', 'Prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (14, 'PRO', 'Prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (15, 'PRO', 'prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (16, 'PRO', 'prova', 'prova', 1);
INSERT INTO sy_roles (id, cdrole, dsrole, rolekey, deleted) VALUES (17, 'PRO', 'Prova', 'prova', 1);

-- Table: sy_roles_functions
CREATE TABLE sy_roles_functions (id INTEGER CONSTRAINT PK_sy_roles_functions PRIMARY KEY ASC AUTOINCREMENT NOT NULL, idrole NUMERIC NOT NULL CONSTRAINT fk_roles_rolesfunctions REFERENCES sy_roles (id), idfunction NUMERIC CONSTRAINT fk_functions_rolesfunctions REFERENCES sy_functions (id) NOT NULL, dtstart DATE NOT NULL, dtstop DATE, CONSTRAINT uk_roles_functions UNIQUE (idrole, idfunction, dtstart));
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (5, 9, 4, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (21, 17, 1, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (22, 17, 2, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (42, 10, 6, '2020-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (43, 2, 6, '2020-01-01 00:00:00', '2021-01-31 00:00:00');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (119, 1, 0, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (120, 1, 1, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (121, 1, 2, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (122, 1, 3, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (123, 1, 4, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (124, 1, 5, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (125, 1, 6, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (126, 1, 7, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (127, 1, 8, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (128, 1, 9, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (129, 1, 10, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (130, 1, 11, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (131, 1, 12, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (132, 1, 13, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (133, 1, 14, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (134, 1, 15, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (135, 1, 16, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_roles_functions (id, idrole, idfunction, dtstart, dtstop) VALUES (136, 1, 17, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');

-- Table: sy_users
CREATE TABLE sy_users (id INTEGER CONSTRAINT PK_sy_users PRIMARY KEY ASC AUTOINCREMENT NOT NULL, username VARCHAR (20) NOT NULL, password VARCHAR (64) NOT NULL, email VARCHAR (50) NOT NULL, name VARCHAR (50) NOT NULL, surname VARCHAR (50) NOT NULL, dtstart DATE NOT NULL, dtstop DATE, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO sy_users (id, username, password, email, name, surname, dtstart, dtstop, deleted) VALUES (1, 'admin', 'admin', 'admin@localhost.com', 'admin', 'admin', '2021-01-01 16:41:09', '2021-12-31 00:00:00', 0);
INSERT INTO sy_users (id, username, password, email, name, surname, dtstart, dtstop, deleted) VALUES (2, 'user1', 'user1', 'user1@localhost.com', 'user1', 'user1', '2021-01-01 00:00:00', '2021-12-31 00:00:00', 0);

-- Table: sy_users_roles
CREATE TABLE sy_users_roles (id INTEGER CONSTRAINT PK_sy_users_roles PRIMARY KEY ASC AUTOINCREMENT NOT NULL, iduser INTEGER CONSTRAINT fk_users_usersroles REFERENCES sy_users (id) NOT NULL, idrole INTEGER CONSTRAINT fk_roles_usersroles REFERENCES sy_roles (id) NOT NULL, dtstart DATE NOT NULL, dtstop DATE);
INSERT INTO sy_users_roles (id, iduser, idrole, dtstart, dtstop) VALUES (2, 0, 2, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_users_roles (id, iduser, idrole, dtstart, dtstop) VALUES (4, 1, 1, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');
INSERT INTO sy_users_roles (id, iduser, idrole, dtstart, dtstop) VALUES (6, 2, 2, '0001-01-01 00:00:00', '9999-12-31 23:59:59.9999999');

-- Table: wh_location_master
CREATE TABLE wh_location_master (id INTEGER CONSTRAINT pk_wh_location_master PRIMARY KEY AUTOINCREMENT NOT NULL, cdlocation VARCHAR (20) NOT NULL CONSTRAINT uk_cdlocation UNIQUE, idlocationtype INTEGER CONSTRAINT fk_location_type REFERENCES wh_location_types (id) NOT NULL, idwarehousemaster INTEGER CONSTRAINT fk_warehouse_master REFERENCES wh_warehouse_master (id) NOT NULL, cdroot VARCHAR (5) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (1, 'MA1.SCAF001.00.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (2, 'MA1.SCAF001.00.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (3, 'MA1.SCAF001.00.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (4, 'MA1.SCAF001.00.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (5, 'MA1.SCAF001.00.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (6, 'MA1.SCAF001.00.05', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (7, 'MA1.SCAF001.01.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (8, 'MA1.SCAF001.01.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (9, 'MA1.SCAF001.01.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (10, 'MA1.SCAF001.01.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (11, 'MA1.SCAF001.01.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (12, 'MA1.SCAF001.01.05', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (13, 'MA1.SCAF001.02.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (14, 'MA1.SCAF001.02.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (15, 'MA1.SCAF001.02.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (16, 'MA1.SCAF001.02.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (17, 'MA1.SCAF001.02.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (18, 'MA1.SCAF001.02.05', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (19, 'MA1.SCAF001.03.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (20, 'MA1.SCAF001.03.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (21, 'MA1.SCAF001.03.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (22, 'MA1.SCAF001.03.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (23, 'MA1.SCAF001.03.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (24, 'MA1.SCAF001.03.05', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (25, 'MA1.SCAF001.04.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (26, 'MA1.SCAF001.04.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (27, 'MA1.SCAF001.04.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (28, 'MA1.SCAF001.04.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (29, 'MA1.SCAF001.04.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (30, 'MA1.SCAF001.04.05', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (31, 'MA1.SCAF001.05.00', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (32, 'MA1.SCAF001.05.01', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (33, 'MA1.SCAF001.05.02', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (34, 'MA1.SCAF001.05.03', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (35, 'MA1.SCAF001.05.04', 2, 1, '001', 0);
INSERT INTO wh_location_master (id, cdlocation, idlocationtype, idwarehousemaster, cdroot, deleted) VALUES (36, 'MA1.SCAF001.05.05', 2, 1, '001', 0);

-- Table: wh_location_types
CREATE TABLE wh_location_types (id INTEGER CONSTRAINT pk_wh_location_types PRIMARY KEY AUTOINCREMENT NOT NULL, cdlocationtype VARCHAR (50) NOT NULL, dslocationtype VARCHAR (100) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_location_types (id, cdlocationtype, dslocationtype, deleted) VALUES (1, 'CAS', 'Cassettiera', 0);
INSERT INTO wh_location_types (id, cdlocationtype, dslocationtype, deleted) VALUES (2, 'SCAF', 'Scaffale', 0);
INSERT INTO wh_location_types (id, cdlocationtype, dslocationtype, deleted) VALUES (3, 'AREA', 'Area di Stoccaggio', 0);

-- Table: wh_material_master
CREATE TABLE wh_material_master (id INTEGER CONSTRAINT pk_wh_material_master PRIMARY KEY AUTOINCREMENT NOT NULL, cdmaterial VARCHAR (20) NOT NULL, dsmaterial VARCHAR (120) NOT NULL, idmaterialtype INTEGER CONSTRAINT fk_materialtype REFERENCES wh_material_types (id) NOT NULL, iduom INTEGER CONSTRAINT fk_uom REFERENCES mt_uom (id) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_material_master (id, cdmaterial, dsmaterial, idmaterialtype, iduom, deleted) VALUES (0, 'M-0001', 'Materiale 1', 1, 2, 0);
INSERT INTO wh_material_master (id, cdmaterial, dsmaterial, idmaterialtype, iduom, deleted) VALUES (1, 'M-0002', 'Materiale 2', 1, 2, 0);
INSERT INTO wh_material_master (id, cdmaterial, dsmaterial, idmaterialtype, iduom, deleted) VALUES (2, 'M-0003', 'Materiale 3', 1, 3, 0);
INSERT INTO wh_material_master (id, cdmaterial, dsmaterial, idmaterialtype, iduom, deleted) VALUES (3, 'M-0004', 'Materiale 4', 1, 1, 0);
INSERT INTO wh_material_master (id, cdmaterial, dsmaterial, idmaterialtype, iduom, deleted) VALUES (4, 'M-0005', 'Materiale 5 - Liquido infiammabile', 1, 3, 0);

-- Table: wh_material_types
CREATE TABLE wh_material_types (id INTEGER CONSTRAINT pk_wh_material_types PRIMARY KEY AUTOINCREMENT NOT NULL, cdmaterialtype VARCHAR (50) NOT NULL, dsmaterialtype VARCHAR (100) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_material_types (id, cdmaterialtype, dsmaterialtype, deleted) VALUES (1, 'ZMAT', 'Materia Prima', 0);
INSERT INTO wh_material_types (id, cdmaterialtype, dsmaterialtype, deleted) VALUES (2, 'ZSEMI', 'Semilavorato', 0);
INSERT INTO wh_material_types (id, cdmaterialtype, dsmaterialtype, deleted) VALUES (3, 'ZPROD', 'Prodotto Finito', 0);

-- Table: wh_stock_types
CREATE TABLE wh_stock_types (id INTEGER CONSTRAINT pk_wh_stock_types PRIMARY KEY AUTOINCREMENT NOT NULL, cdstocktype VARCHAR (50) NOT NULL, dsstocktype VARCHAR (100) NOT NULL, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_stock_types (id, cdstocktype, dsstocktype, deleted) VALUES (1, 'LIFO', 'LIFO', 1);
INSERT INTO wh_stock_types (id, cdstocktype, dsstocktype, deleted) VALUES (2, 'FIFO', 'FIFO', 1);
INSERT INTO wh_stock_types (id, cdstocktype, dsstocktype, deleted) VALUES (3, 'ZPAR', 'A Partita', 0);
INSERT INTO wh_stock_types (id, cdstocktype, dsstocktype, deleted) VALUES (4, 'ZLIB', 'Stock Libero', 0);

-- Table: wh_storage_order
CREATE TABLE wh_storage_order (id INTEGER CONSTRAINT pk_wh_storage_order PRIMARY KEY AUTOINCREMENT NOT NULL, idmaterial INTEGER CONSTRAINT fk_wh_material_master REFERENCES wh_material_master (id) NOT NULL, idwarehousemaster INTEGER CONSTRAINT fk_wh_warehouse_master REFERENCES wh_warehouse_master (id) NOT NULL, quantity DECIMAL NOT NULL, state VARCHAR (20) NOT NULL DEFAULT INSERITO, deleted BOOLEAN NOT NULL DEFAULT (0));
INSERT INTO wh_storage_order (id, idmaterial, idwarehousemaster, quantity, state, deleted) VALUES (1, 3, 1, 2, 'INSERITO', 0);
INSERT INTO wh_storage_order (id, idmaterial, idwarehousemaster, quantity, state, deleted) VALUES (2, 1, 2, 10, 'INSERITO', 0);

-- Table: wh_warehouse_master
CREATE TABLE wh_warehouse_master (id INTEGER CONSTRAINT pk_wh_warehouse_master PRIMARY KEY AUTOINCREMENT NOT NULL, cdwarehousemaster VARCHAR (50) NOT NULL, dswarehousemaster VARCHAR (100) NOT NULL, deleted BOOLEAN DEFAULT (0) NOT NULL);
INSERT INTO wh_warehouse_master (id, cdwarehousemaster, dswarehousemaster, deleted) VALUES (1, 'MA1', 'Magazzino 1', 0);
INSERT INTO wh_warehouse_master (id, cdwarehousemaster, dswarehousemaster, deleted) VALUES (2, 'MA2', 'Magazzino 2', 0);
INSERT INTO wh_warehouse_master (id, cdwarehousemaster, dswarehousemaster, deleted) VALUES (3, 'AT', 'Area a Terra', 0);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
