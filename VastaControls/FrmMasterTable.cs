﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VastaControls
{
    public partial class FrmMasterTable : Form
    {
        protected BindingSource bs = new BindingSource();
        protected Boolean addMode = false;
        protected Boolean bModified = false;
        protected String dataPropCode;
        protected String dataPropDescription;

        public FrmMasterTable()
        {
            InitializeComponent();
        }
        protected void FormLoad(DataTable dt) 
        {
            try
            {
                dgvList.AutoGenerateColumns = false;

                bs.DataSource = dt;
                dgvList.DataSource = bs;

                txtCode.DataBindings.Add("Text", bs, dataPropCode);
                txtDescription.DataBindings.Add("Text", bs, dataPropDescription);

                dgvList.Columns["code"].DataPropertyName = dataPropCode;
                dgvList.Columns["description"].DataPropertyName = dataPropDescription;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        protected virtual void Save() { }
        protected virtual void Insert() { }
        protected virtual void Delete() { }
        protected virtual void dgvList_SelectionChanged(object sender, EventArgs e) { }
        protected virtual void dgvList_RowValidating(object sender, DataGridViewCellCancelEventArgs e) { }
        //
        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Confermi il salvataggio?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (res == DialogResult.Yes)
            {
                Save();
                bModified = false;
            }
        }
        //
        private void btnInsert_Click(object sender, EventArgs e)
        {
            Insert();
        }
        //
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Confermi la cancellazione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (res == DialogResult.Yes)
                Delete();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (bModified == true)
            { 
                DialogResult res = MessageBox.Show("I dati non salvati saranno persi! Confermi?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.No)
                    e.Cancel = true;
            }
        }
        //

        private void txtCode_TextChanged(object sender, EventArgs e)
        {
            DataRowView CurrentRow = (DataRowView)bs.Current;
            if (CurrentRow != null)
            {
                if (CurrentRow.Row[dataPropCode].ToString() != txtCode.Text)
                    bModified = true;
                else
                    bModified = false;
            }
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            DataRowView CurrentRow = (DataRowView)bs.Current;
            if (CurrentRow != null)
            {
                if (CurrentRow.Row[dataPropDescription].ToString() != txtDescription.Text)
                    bModified = true;
                else
                    bModified = false;
            }
        }

        //
    }
}
