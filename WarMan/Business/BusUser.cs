using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusUser : BusBase
    {
        public Int32 idRole;
        public String userName;

        private DSWarMan.sy_usersDataTable _dtUsers;
        private DSWarMan.sy_users_rolesDataTable _dtUsers_Roles;
        private DSWarMan.sy_groups_usersDataTable _dtUsers_Groups;
        private DaUser _oDa;

        public BusUser()
        {
            _dtUsers = new DSWarMan.sy_usersDataTable();
            _dtUsers_Roles = new DSWarMan.sy_users_rolesDataTable();
            _dtUsers_Groups = new DSWarMan.sy_groups_usersDataTable();
        }

        public DSWarMan.sy_usersDataTable getList()
        {
            try
            {
                _oDa = new DaUser(_dtUsers);
                _dtUsers = (DSWarMan.sy_usersDataTable)_oDa.Select("deleted <> 1");

                // campo calcolato
                _dtUsers.Columns.Add("active");
                DateTime oggi = DateTime.Now;
                foreach (DSWarMan.sy_usersRow row in _dtUsers.Rows)
                {
                    if (row.dtstart <= oggi && row.dtstop >= oggi)                    
                        row["active"] = "X";
                    else
                        row["active"] = "";
                }

                return _dtUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DataTable getUserByName(String UserName)
        {
            _oDa = new DaUser(_dtUsers);
            _dtUsers = (DSWarMan.sy_usersDataTable)_oDa.Select("deleted <> 1 AND username = '" + UserName + "'");

            return _dtUsers;
        }

        //
        public DataTable getRolesByUser()
        {
            try
            {
                _oDa = new DaUser(_dtUsers_Roles);
                DataTable dt = _oDa.SelectRolesByUser(this.id);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DataTable getGroupsByUser()
        {
            try
            {
                _oDa = new DaUser(_dtUsers_Groups);
                DataTable dt = _oDa.SelectGroupsByUser(this.id);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaUser(_dtUsers);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtUsers.AcceptChanges();
            }
            catch (Exception ex)
            {
                if (ex.Message != "Nothing to save")
                    throw ex;
            }
        }

        //
        public long getLastInsertId()
        {
            return _oDa.lastInsertRowId;
        }

        //
        public void saveUsersRoles(Int32 idUser, DataTable dtRoles)
        {
            try
            {
                _dtUsers_Roles = new DSWarMan.sy_users_rolesDataTable();

                foreach (DataRow dr in dtRoles.Rows)
                {
                    if (dr["enabled"] != DBNull.Value && Convert.ToBoolean(dr["enabled"]))
                    {
                        DSWarMan.sy_users_rolesRow row = (DSWarMan.sy_users_rolesRow)_dtUsers_Roles.NewRow();
                        row.iduser = idUser;
                        row.idrole = Convert.ToInt32(dr["idrole"]);
                        row.dtstart = dr["dtstart"] != DBNull.Value ? Convert.ToDateTime(dr["dtstart"]) : DateTime.MinValue;
                        row.dtstop = dr["dtstop"] != DBNull.Value ? Convert.ToDateTime(dr["dtstop"]) : DateTime.MaxValue;

                        _dtUsers_Roles.Addsy_users_rolesRow(row);
                    }
                }

                _oDa = new DaUser(_dtUsers_Roles);
                _oDa.DeleteRolesByUser(idUser);
                _oDa = new DaUser(_dtUsers_Roles);
                _oDa.Insert();

                _dtUsers_Roles.AcceptChanges();
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        //
        public void saveUsersGroups(Int32 idUser, DataTable dtGroups)
        {
            try
            {
                _dtUsers_Groups = new DSWarMan.sy_groups_usersDataTable();

                foreach (DataRow dr in dtGroups.Rows)
                {
                    if (dr["enabled"] != DBNull.Value && Convert.ToBoolean(dr["enabled"]))
                    {
                        DSWarMan.sy_groups_usersRow row = (DSWarMan.sy_groups_usersRow)_dtUsers_Groups.NewRow();
                        row.iduser = idUser;
                        row.idgroup = Convert.ToInt32(dr["idgroup"]);
                        row.dtstart = dr["dtstart"] != DBNull.Value ? Convert.ToDateTime(dr["dtstart"]) : DateTime.MinValue;
                        row.dtstop = dr["dtstop"] != DBNull.Value ? Convert.ToDateTime(dr["dtstop"]) : DateTime.MaxValue;

                        _dtUsers_Groups.Addsy_groups_usersRow(row);
                    }
                }

                _oDa = new DaUser(_dtUsers_Groups);
                _oDa.DeleteGroupsByUser(idUser);
                _oDa = new DaUser(_dtUsers_Groups);
                _oDa.Insert();

                _dtUsers_Groups.AcceptChanges();
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        //
    }
}
