using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusRole : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.sy_rolesDataTable _dtRoles;
        private DSWarMan.sy_roles_functionsDataTable _dtRoles_functions;
        private DaRole _oDa;


        public BusRole()
        {
            _dtRoles = new DSWarMan.sy_rolesDataTable();
            _dtRoles_functions = new DSWarMan.sy_roles_functionsDataTable();
        }

        public DSWarMan.sy_rolesDataTable getList()
        {
            try
            {
                _oDa = new DaRole(_dtRoles);
                _dtRoles = (DSWarMan.sy_rolesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtRoles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public DataTable getFunctionsByRole()
        {
            try
            {
                _oDa = new DaRole(_dtRoles_functions);
                DataTable dt = _oDa.SelectFunctionsByRole(this.id);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaRole(_dtRoles);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtRoles.AcceptChanges();
            }
            catch (Exception ex)
            {
                if (ex.Message != "Nothing to save")
                    throw ex;
            }
        }

        //
        public long getLastInsertId()
        {
            return _oDa.lastInsertRowId;
        }

        //
        public void saveRolesFunctions(Int32 idRole, DataTable dtFunctions)
        {
            try
            {
                _dtRoles_functions = new DSWarMan.sy_roles_functionsDataTable();

                foreach (DataRow dr in dtFunctions.Rows)
                {
                    if (dr["enabled"] != DBNull.Value && Convert.ToBoolean(dr["enabled"]))
                    {
                        DSWarMan.sy_roles_functionsRow row = (DSWarMan.sy_roles_functionsRow)_dtRoles_functions.NewRow();
                        row.idrole = idRole;
                        row.idfunction = Convert.ToInt32(dr["idfunction"]);
                        row.dtstart = dr["dtstart"] != DBNull.Value ? Convert.ToDateTime(dr["dtstart"]) : DateTime.MinValue;
                        row.dtstop = dr["dtstop"] != DBNull.Value ? Convert.ToDateTime(dr["dtstop"]) : DateTime.MaxValue;

                        _dtRoles_functions.Addsy_roles_functionsRow(row);
                    }
                }

                _oDa = new DaRole(_dtRoles_functions);
                _oDa.DeleteFunctionByRole(idRole);
                _oDa = new DaRole(_dtRoles_functions);
                _oDa.Insert();

                _dtRoles_functions.AcceptChanges();
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        //
    }
}
