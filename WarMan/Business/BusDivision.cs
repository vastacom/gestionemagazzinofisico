﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;
/*
 * sistemare relazione su db e su xsd con cm_companies
 * **/
namespace WarMan.Business
{
    public class BusDivision : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.cm_divisionsDataTable _dtDivisions;
        private DaDivision _oDa;

        public BusDivision()
        {
            _dtDivisions = new DSWarMan.cm_divisionsDataTable();
        }

        public DSWarMan.cm_divisionsDataTable getList(String strFilter)
        {
            try
            {
                _oDa = new DaDivision(_dtDivisions);
                _dtDivisions = (DSWarMan.cm_divisionsDataTable)_oDa.Select(strFilter);

                return _dtDivisions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaDivision(_dtDivisions);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtDivisions.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
