using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusUom : BusBase
    {
        public String code;
        public String description;
        public String symbol;

        private DSWarMan.mt_uomDataTable _dtUom;
        private DaUom _oDa;

        public BusUom()
        {
            _dtUom = new DSWarMan.mt_uomDataTable();
        }

        public DSWarMan.mt_uomDataTable getList()
        {
            try
            {
                _oDa = new DaUom(_dtUom);
                _dtUom = (DSWarMan.mt_uomDataTable)_oDa.Select("deleted <> 1");
               
                return _dtUom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                DSWarMan.mt_uomDataTable dt = new DSWarMan.mt_uomDataTable();
                _oDa = new DaUom(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.mt_uomDataTable)_oDa.Select(strFilter);
                DSWarMan.mt_uomRow row = (DSWarMan.mt_uomRow)dt.Rows[0];
                this.code = row.cduom;
                this.description = row.dsuom;
                this.symbol = row.uomsymbol;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaUom(_dtUom);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtUom.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
