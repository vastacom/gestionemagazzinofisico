using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusPickingOrder : BusBase
    {
        public Int32 idMateriale;
        public Int32 idMagazzino;
        public Decimal quantita;
        public Decimal qtaEseguita;
        public String stato;

        private DSWarMan.wh_picking_orderDataTable _dtPickingOrder;
        private DaPickingOrder _oDa;

        public BusPickingOrder()
        {
            _dtPickingOrder = new DSWarMan.wh_picking_orderDataTable();
        }

        public DSWarMan.wh_picking_orderDataTable getList()
        {
            try
            {
                _oDa = new DaPickingOrder(_dtPickingOrder);
                _dtPickingOrder = (DSWarMan.wh_picking_orderDataTable)_oDa.Select("deleted <> 1");
               
                return _dtPickingOrder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DataTable search(String strMateriale, Int32 idMagazzino, String strStato)
        {
            try
            {
                _oDa = new DaPickingOrder(_dtPickingOrder);
                DataTable dt = _oDa.search(strMateriale, idMagazzino, strStato);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                _oDa = new DaPickingOrder(_dtPickingOrder);
                _dtPickingOrder = (DSWarMan.wh_picking_orderDataTable)_oDa.Select("id = " + this.id + " AND deleted <> 1");

                if (_dtPickingOrder.Rows.Count > 0)
                {
                    this.idMateriale = Convert.ToInt32(_dtPickingOrder.Rows[0]["idmaterial"]);
                    this.idMagazzino = Convert.ToInt32(_dtPickingOrder.Rows[0]["idwarehousemaster"]);
                    this.quantita = Convert.ToDecimal(_dtPickingOrder.Rows[0]["quantity"]);
                    this.qtaEseguita = Convert.ToDecimal(_dtPickingOrder.Rows[0]["quantityexec"]);
                    this.stato = _dtPickingOrder.Rows[0]["state"].ToString();
                }                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void save(Boolean addMode)
        {
            try
            {
                DSWarMan.wh_picking_orderDataTable dt = new DSWarMan.wh_picking_orderDataTable();
                if (addMode)
                {
                    DSWarMan.wh_picking_orderRow row = (DSWarMan.wh_picking_orderRow)dt.NewRow();
                    row.idmaterial = this.idMateriale;
                    row.idwarehousemaster = this.idMagazzino;
                    row.quantity = this.quantita;
                    row.quantityexec = 0;
                    row.state = this.stato;
                    row.deleted = false;
                    dt.Addwh_picking_orderRow(row);

                    _oDa = new DaPickingOrder(dt);
                    _oDa.Insert();
                    this.id = Convert.ToInt32(_oDa.lastInsertRowId);
                }
                else
                {
                    _oDa = new DaPickingOrder(dt);
                    String strFilter = "id = " + this.id + " AND deleted <> 1";
                    dt = (DSWarMan.wh_picking_orderDataTable)_oDa.Select(strFilter);
                    DSWarMan.wh_picking_orderRow row = (DSWarMan.wh_picking_orderRow)dt.Rows[0];
                    row.idmaterial = this.idMateriale;
                    row.idwarehousemaster = this.idMagazzino;
                    row.quantity = this.quantita;
                    row.quantityexec = this.qtaEseguita;
                    row.state = this.stato;

                    _oDa = new DaPickingOrder(dt);
                    _oDa.Update();
                }
                dt.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void execute(Int32 idLocation, Decimal qtaEseguita)
        {
            try
            {
                // Giacenza
                BusStock busStock = new BusStock();
                busStock.idMaterial = this.idMateriale;
                busStock.idWarehouseMaster = this.idMagazzino;
                busStock.idLocation = idLocation;
                busStock.checkExist();

                if (busStock.id > 0)
                {
                    // Giacenza gi� esistente
                    if (busStock.quantity >= qtaEseguita)
                    {
                        busStock.quantity -= qtaEseguita;
                        busStock.dateLastMovement = DateTime.Now;
                        busStock.save(false);
                    }
                    else
                    {
                        // Errore: giacenza insufficiente
                        throw new Exception("Manca giacenza!");
                    }
                }
                else
                {
                    // Errore: non c'� giacenza
                    throw new Exception("Manca giacenza!");
                }

                // Stampa Cartellino
                // TODO


                // Movimento
                BusHandling busHandling = new BusHandling();
                busHandling.idMaterial = this.idMateriale;
                busHandling.idWarehouseMaster = this.idMagazzino;
                busHandling.idLocation = idLocation;
                busHandling.quantity = qtaEseguita;
                busHandling.dateHandling = DateTime.Now;
                busHandling.typeHandling = "PRELIEVO";

                busHandling.save(true);

                // Cambio di stato
                if (this.qtaEseguita < this.quantita)
                    this.stato = "ESEGUITO PARZ.";
                else
                    this.stato = "ESEGUITO";

                this.save(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
    }
}
