using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusLanguage : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.mt_languagesDataTable _dtLanguages;
        private DaLanguage _oDa;

        public BusLanguage()
        {
            _dtLanguages = new DSWarMan.mt_languagesDataTable();
        }

        public DSWarMan.mt_languagesDataTable getList()
        {
            try
            {
                _oDa = new DaLanguage(_dtLanguages);
                _dtLanguages = (DSWarMan.mt_languagesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtLanguages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                DSWarMan.mt_languagesDataTable dt = new DSWarMan.mt_languagesDataTable();
                _oDa = new DaLanguage(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.mt_languagesDataTable)_oDa.Select(strFilter);
                DSWarMan.mt_languagesRow row = (DSWarMan.mt_languagesRow)dt.Rows[0];
                this.code = row.cdlanguage;
                this.description = row.dslanguage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaLanguage(_dtLanguages);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtLanguages.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
