﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusCompany : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.cm_companiesDataTable _dtCompanies;
        private DaCompany _oDa;

        public BusCompany()
        {
            _dtCompanies = new DSWarMan.cm_companiesDataTable();
        }

        public DSWarMan.cm_companiesDataTable getList(String strFilter)
        {
            try
            {
                _oDa = new DaCompany(_dtCompanies);
                _dtCompanies = (DSWarMan.cm_companiesDataTable)_oDa.Select(strFilter);

                return _dtCompanies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {  
            try
            {
                _oDa = new DaCompany(_dtCompanies);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtCompanies.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
