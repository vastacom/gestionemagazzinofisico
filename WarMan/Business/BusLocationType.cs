using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusLocationType : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.wh_location_typesDataTable _dtLocationType;
        private DaLocationType _oDa;

        public BusLocationType()
        {
            _dtLocationType = new DSWarMan.wh_location_typesDataTable();
        }

        public DSWarMan.wh_location_typesDataTable getList()
        {
            try
            {
                _oDa = new DaLocationType(_dtLocationType);
                _dtLocationType = (DSWarMan.wh_location_typesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtLocationType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void getDetailsById()
        {
            try
            {
                DSWarMan.wh_location_typesDataTable dt = new DSWarMan.wh_location_typesDataTable();
                _oDa = new DaLocationType(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.wh_location_typesDataTable)_oDa.Select(strFilter);
                DSWarMan.wh_location_typesRow row = (DSWarMan.wh_location_typesRow)dt.Rows[0];
                this.code = row.cdlocationtype;
                this.description = row.dslocationtype;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaLocationType(_dtLocationType);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtLocationType.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
