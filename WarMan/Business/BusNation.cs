using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusNation : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.mt_nationsDataTable _dtNations;
        private DaNation _oDa;

        public BusNation()
        {
            _dtNations = new DSWarMan.mt_nationsDataTable();
        }

        public DSWarMan.mt_nationsDataTable getList()
        {
            try
            {
                _oDa = new DaNation(_dtNations);
                _dtNations = (DSWarMan.mt_nationsDataTable)_oDa.Select("deleted <> 1");
               
                return _dtNations;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaNation(_dtNations);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtNations.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
