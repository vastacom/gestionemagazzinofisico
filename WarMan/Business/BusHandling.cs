using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusHandling : BusBase
    {
        public Int32 idMaterial;
        public Int32 idWarehouseMaster;
        public Int32 idLocation;
        public Decimal quantity;
        public DateTime dateHandling;
        public String typeHandling;

        private DSWarMan.wh_handlingDataTable _dtHandling;
        private DSWarMan.view_handlingDataTable _dtVoucher;
        private DaHandling _oDa;

        public BusHandling()
        {
            _dtHandling = new DSWarMan.wh_handlingDataTable();
            _dtVoucher = new DSWarMan.view_handlingDataTable();
        }

        public DSWarMan.wh_handlingDataTable getList()
        {
            try
            {
                _oDa = new DaHandling(_dtHandling);
                _dtHandling = (DSWarMan.wh_handlingDataTable)_oDa.Select("deleted <> 1");
               
                return _dtHandling;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DataTable search(String strMaterialCode, DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                _oDa = new DaHandling(_dtHandling);
                DataTable dt = (DSWarMan.view_handlingDataTable)_oDa.search(this.idWarehouseMaster, strMaterialCode, dtFrom, dtTo, 0);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getRowById()
        {
            try
            {
                _oDa = new DaHandling(_dtHandling);
                _dtHandling = (DSWarMan.wh_handlingDataTable)_oDa.Select("deleted <> 1 AND id = " + this.id.ToString());

                return _dtHandling;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public DataTable getVoucherById()
        {
            try
            {
                _oDa = new DaHandling(_dtVoucher);
                _dtVoucher = (DSWarMan.view_handlingDataTable)_oDa.search(0, "", DateTime.MinValue, DateTime.MinValue, this.id);

                return _dtVoucher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                DSWarMan.wh_handlingDataTable dt = new DSWarMan.wh_handlingDataTable();
                if (addMode)
                {
                    DSWarMan.wh_handlingRow row = (DSWarMan.wh_handlingRow)dt.NewRow();
                    row.idmaterial = this.idMaterial;
                    row.idwarehouse = this.idWarehouseMaster;
                    row.idlocation = this.idLocation;
                    row.quantity = this.quantity;
                    row.datehandling = this.dateHandling;
                    row.typehandling = this.typeHandling;
                    row.deleted = false;
                    dt.Addwh_handlingRow(row);

                    _oDa = new DaHandling(dt);
                    _oDa.Insert();
                    this.id = Convert.ToInt32(_oDa.lastInsertRowId);   
                }
                else
                {
                    // Si lavora solo in inserimento
                }
                dt.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
