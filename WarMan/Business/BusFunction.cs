using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusFunction : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.sy_functionsDataTable _dtFunctions;
        private DaFunction _oDa;

        public BusFunction()
        {
            _dtFunctions = new DSWarMan.sy_functionsDataTable();
        }

        public DSWarMan.sy_functionsDataTable getList()
        {
            try
            {
                _oDa = new DaFunction(_dtFunctions);
                _dtFunctions = (DSWarMan.sy_functionsDataTable)_oDa.Select("deleted <> 1");
               
                return _dtFunctions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaFunction(_dtFunctions);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtFunctions.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DSWarMan.sy_functionsDataTable getListByRole(Int32 idRole)
        {
            try
            {
                _oDa = new DaFunction(_dtFunctions);
                _dtFunctions = (DSWarMan.sy_functionsDataTable)_oDa.SelectByRole(idRole);

                return _dtFunctions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public String getFormNameByFunctionKey(String strKey)
        {
            try
            {
                _oDa = new DaFunction(_dtFunctions);
                _dtFunctions = (DSWarMan.sy_functionsDataTable)_oDa.Select("functionkey='" + strKey + "'");

                if (_dtFunctions.Rows.Count > 0)
                    return _dtFunctions.Rows[0]["var2"].ToString();
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
    }
}
