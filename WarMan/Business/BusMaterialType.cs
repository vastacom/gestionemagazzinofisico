using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusMaterialType : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.wh_material_typesDataTable _dtMaterialTypes;
        private DaMaterialType _oDa;

        public BusMaterialType()
        {
            _dtMaterialTypes = new DSWarMan.wh_material_typesDataTable();
        }

        public DSWarMan.wh_material_typesDataTable getList()
        {
            try
            {
                _oDa = new DaMaterialType(_dtMaterialTypes);
                _dtMaterialTypes = (DSWarMan.wh_material_typesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtMaterialTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaMaterialType(_dtMaterialTypes);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtMaterialTypes.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
