using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusCurrency : BusBase
    {
        public String code;
        public String description;
        public String symbol;

        private DSWarMan.mt_currenciesDataTable _dtCurrencies;
        private DaCurrency _oDa;

        public BusCurrency()
        {
            _dtCurrencies = new DSWarMan.mt_currenciesDataTable();
        }

        public DSWarMan.mt_currenciesDataTable getList()
        {
            try
            {
                _oDa = new DaCurrency(_dtCurrencies);
                _dtCurrencies = (DSWarMan.mt_currenciesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtCurrencies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                DSWarMan.mt_currenciesDataTable dt = new DSWarMan.mt_currenciesDataTable();
                _oDa = new DaCurrency(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.mt_currenciesDataTable)_oDa.Select(strFilter);
                DSWarMan.mt_currenciesRow row = (DSWarMan.mt_currenciesRow)dt.Rows[0];
                this.code = row.cdcurrency;
                this.description = row.dscurrency;
                this.symbol = row.currencysymbol;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaCurrency(_dtCurrencies);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtCurrencies.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
