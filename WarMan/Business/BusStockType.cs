using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusStockType : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.wh_stock_typesDataTable _dtStockTypes;
        private DaStockType _oDa;

        public BusStockType()
        {
            _dtStockTypes = new DSWarMan.wh_stock_typesDataTable();
        }

        public DSWarMan.wh_stock_typesDataTable getList()
        {
            try
            {
                _oDa = new DaStockType(_dtStockTypes);
                _dtStockTypes = (DSWarMan.wh_stock_typesDataTable)_oDa.Select("deleted <> 1");
               
                return _dtStockTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaStockType(_dtStockTypes);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtStockTypes.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
