using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusGroup : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.sy_groupsDataTable _dtGroup;
        private DaGroup _oDa;

        public BusGroup()
        {
            _dtGroup = new DSWarMan.sy_groupsDataTable();
        }

        public DSWarMan.sy_groupsDataTable getList()
        {
            try
            {
                _oDa = new DaGroup(_dtGroup);
                _dtGroup = (DSWarMan.sy_groupsDataTable)_oDa.Select("deleted <> 1");
               
                return _dtGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaGroup(_dtGroup);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtGroup.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
