using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusStock : BusBase
    {
        public Int32 idMaterial;
        public Int32 idWarehouseMaster;
        public Int32 idLocation;
        public Decimal quantity;
        public DateTime dateLastMovement;

        private DSWarMan.wh_stocksDataTable _dtStock;
        private DaStock _oDa;

        public BusStock()
        {
            _dtStock = new DSWarMan.wh_stocksDataTable();
        }

        public DSWarMan.wh_stocksDataTable getList()
        {
            try
            {
                _oDa = new DaStock(_dtStock);
                _dtStock = (DSWarMan.wh_stocksDataTable)_oDa.Select("deleted <> 1");
               
                return _dtStock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public DataTable search(String strMaterialCode)
        {
            try
            {
                _oDa = new DaStock(_dtStock);
                DataTable dt = _oDa.search(this.idWarehouseMaster, strMaterialCode);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void checkExist()
        {
            try
            {
                _oDa = new DaStock(_dtStock);
                String strFilter = "deleted <> 1";
                strFilter += " AND idmaterial = " + this.idMaterial;
                strFilter += " AND idwarehousemaster = " + this.idWarehouseMaster;
                strFilter += " AND idlocation = " + this.idLocation;

                _dtStock = (DSWarMan.wh_stocksDataTable)_oDa.Select(strFilter);
                if (_dtStock.Rows.Count > 0)
                {
                    DSWarMan.wh_stocksRow row = (DSWarMan.wh_stocksRow)_dtStock.Rows[0];

                    this.id = row.id;
                    this.quantity = row.quantity;
                    this.dateLastMovement = row.datelastmovement;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                DSWarMan.wh_stocksDataTable dt = new DSWarMan.wh_stocksDataTable();
                if (addMode)
                {
                    DSWarMan.wh_stocksRow row = (DSWarMan.wh_stocksRow)dt.NewRow();
                    row.idmaterial = this.idMaterial;
                    row.idwarehousemaster = this.idWarehouseMaster;
                    row.idlocation = this.idLocation;
                    row.quantity = this.quantity;
                    row.datelastmovement = this.dateLastMovement;
                    row.deleted = false;
                    dt.Addwh_stocksRow(row);

                    _oDa = new DaStock(dt);
                    _oDa.Insert();
                    this.id = Convert.ToInt32(_oDa.lastInsertRowId);   
                }
                else
                {
                    _oDa = new DaStock(dt);
                    String strFilter = "id = " + this.id + " AND deleted <> 1";
                    dt = (DSWarMan.wh_stocksDataTable)_oDa.Select(strFilter);
                    DSWarMan.wh_stocksRow row = (DSWarMan.wh_stocksRow)dt.Rows[0];
                    row.quantity = this.quantity;
                    row.datelastmovement = this.dateLastMovement;

                    _oDa = new DaStock(dt);
                    _oDa.Update();
                }
                dt.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
