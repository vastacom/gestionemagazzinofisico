using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusLocationMaster : BusBase
    {
        public String code;
        public String description;
        public Int32 idLocationType;
        public Int32 idWarehouseMaster;

        private DSWarMan.wh_location_masterDataTable _dtLocationMaster;
        private DaLocationMaster _oDa;

        public BusLocationMaster()
        {
            _dtLocationMaster = new DSWarMan.wh_location_masterDataTable();
        }

        public DSWarMan.wh_location_masterDataTable getList()
        {
            try
            {
                _oDa = new DaLocationMaster(_dtLocationMaster);
                _dtLocationMaster = (DSWarMan.wh_location_masterDataTable)_oDa.Select("deleted <> 1");
               
                return _dtLocationMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public DataTable getListByFilters(Int32 idWarehouseMaster, 
            Int32 idLocationType, String cdRoot)
        {
            try
            {
                _oDa = new DaLocationMaster(_dtLocationMaster);
                DataTable dt = _oDa.SelectByFilters(idWarehouseMaster, idLocationType, cdRoot);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                DSWarMan.wh_location_masterDataTable dt = new DSWarMan.wh_location_masterDataTable();
                _oDa = new DaLocationMaster(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.wh_location_masterDataTable)_oDa.Select(strFilter);
                DSWarMan.wh_location_masterRow row = (DSWarMan.wh_location_masterRow)dt.Rows[0];
                this.code = row.cdlocation;
                this.description = row.cdroot;
                this.idLocationType = row.idlocationtype;
                this.idWarehouseMaster = row.idwarehousemaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaLocationMaster(_dtLocationMaster);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtLocationMaster.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void generateLocation(Int32 idWarehouse, Int32 idLocationType, 
            String codiceRadice, Int32 numPiani, Int32 numFile)
        {
            _dtLocationMaster = new DSWarMan.wh_location_masterDataTable();
            
            // loop sui piani
            for (Int32 currPiano = 0; currPiano <= numPiani; currPiano++)
            {
                // per ogni piano loop sulle file
                for (Int32 currFila = 0; currFila <= numFile; currFila++)
                {
                    String codiceUbicazione = String.Empty;
                    // Codice Magazzino
                    BusWarehouseMaster busWarehouseMaster = new BusWarehouseMaster();
                    busWarehouseMaster.id = idWarehouse;
                    busWarehouseMaster.getDetailsById();
                    BusLocationType busLocationType = new BusLocationType();
                    busLocationType.id = idLocationType;
                    busLocationType.getDetailsById();

                    codiceUbicazione = busWarehouseMaster.code + "." + busLocationType.code + codiceRadice + "." + 
                        currPiano.ToString().PadLeft(2, '0') + "." + currFila.ToString().PadLeft(2, '0');

                    DSWarMan.wh_location_masterRow row = (DSWarMan.wh_location_masterRow)_dtLocationMaster.NewRow();
                    row.idwarehousemaster = idWarehouse;
                    row.idlocationtype = idLocationType;
                    row.cdroot = codiceRadice;
                    row.cdlocation = codiceUbicazione;
                    row.deleted = false;

                    _dtLocationMaster.Addwh_location_masterRow(row);
                }
            }

            _oDa = new DaLocationMaster(_dtLocationMaster);
            _oDa.Insert();

            _dtLocationMaster.AcceptChanges();
        }

        //

    }
}
