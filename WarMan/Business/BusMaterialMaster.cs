using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusMaterialMaster : BusBase
    {
        public String code;
        public String description;
        public Int32 idType;
        public Int32 idUom;

        private DSWarMan.wh_material_masterDataTable _dtMaterialMaster;
        private DaMaterialMaster _oDa;

        public BusMaterialMaster()
        {
            _dtMaterialMaster = new DSWarMan.wh_material_masterDataTable();
        }

        public DataTable search()
        {
            try
            {
                _oDa = new DaMaterialMaster(_dtMaterialMaster);
                DataTable dt = _oDa.search(this.code, this.description, this.idType);
               
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public void getDetailsById()
        {
            try
            {
                DSWarMan.wh_material_masterDataTable dt = new DSWarMan.wh_material_masterDataTable();
                _oDa = new DaMaterialMaster(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.wh_material_masterDataTable)_oDa.Select(strFilter);
                DSWarMan.wh_material_masterRow row = (DSWarMan.wh_material_masterRow)dt.Rows[0];
                this.code = row.cdmaterial;
                this.description = row.dsmaterial;
                this.idType = row.idmaterialtype;
                this.idUom = row.iduom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        public Boolean checkMaterialExists()
        {
            _oDa = new DaMaterialMaster(_dtMaterialMaster);
            String strFilter = "deleted <> 1";
            if (this.code != String.Empty)
                strFilter += " AND cdmaterial = '" + this.code + "'";

            _dtMaterialMaster = (DSWarMan.wh_material_masterDataTable)_oDa.Select(strFilter);
            if (_dtMaterialMaster.Rows.Count > 0)
                return true;
            else
                return false;
        }

        //
        public void save(Boolean addMode)
        {
            try
            {
                DSWarMan.wh_material_masterDataTable dt = new DSWarMan.wh_material_masterDataTable();
                if (addMode)
                {
                    DSWarMan.wh_material_masterRow row = (DSWarMan.wh_material_masterRow)dt.NewRow();
                    row.cdmaterial = this.code;
                    row.dsmaterial = this.description;
                    row.idmaterialtype = this.idType;
                    row.iduom = this.idUom;
                    row.deleted = false;
                    dt.Addwh_material_masterRow(row);

                    _oDa = new DaMaterialMaster(dt);
                    _oDa.Insert();
                    this.id = Convert.ToInt32(_oDa.lastInsertRowId);
                }
                else
                {
                    _oDa = new DaMaterialMaster(dt);
                    String strFilter = "id = " + this.id + " AND deleted <> 1";
                    dt = (DSWarMan.wh_material_masterDataTable)_oDa.Select(strFilter);
                    DSWarMan.wh_material_masterRow row = (DSWarMan.wh_material_masterRow)dt.Rows[0];
                    row.dsmaterial = this.description;
                    row.idmaterialtype = this.idType;
                    row.iduom = this.idUom;

                    _oDa = new DaMaterialMaster(dt);
                    _oDa.Update();
                }
                dt.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

    }
}
