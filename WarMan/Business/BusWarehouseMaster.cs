using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using WarMan.DataAccess;

namespace WarMan.Business
{
    public class BusWarehouseMaster : BusBase
    {
        public String code;
        public String description;

        private DSWarMan.wh_warehouse_masterDataTable _dtWarehouseMaster;
        private DaWarehouseMaster _oDa;

        public BusWarehouseMaster()
        {
            _dtWarehouseMaster = new DSWarMan.wh_warehouse_masterDataTable();
        }

        public DSWarMan.wh_warehouse_masterDataTable getList()
        {
            try
            {
                _oDa = new DaWarehouseMaster(_dtWarehouseMaster);
                _dtWarehouseMaster = (DSWarMan.wh_warehouse_masterDataTable)_oDa.Select("deleted <> 1");
               
                return _dtWarehouseMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //

        public void getDetailsById()
        {
            try
            {
                DSWarMan.wh_warehouse_masterDataTable dt = new DSWarMan.wh_warehouse_masterDataTable();
                _oDa = new DaWarehouseMaster(dt);
                String strFilter = "id = " + this.id + " AND deleted <> 1";
                dt = (DSWarMan.wh_warehouse_masterDataTable)_oDa.Select(strFilter);
                DSWarMan.wh_warehouse_masterRow row = (DSWarMan.wh_warehouse_masterRow)dt.Rows[0];
                this.code = row.cdwarehousemaster;
                this.description = row.dswarehousemaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void save(Boolean addMode)
        {
            try
            {
                _oDa = new DaWarehouseMaster(_dtWarehouseMaster);
                if (addMode)
                    _oDa.Insert();
                else
                    _oDa.Update();

                _dtWarehouseMaster.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
