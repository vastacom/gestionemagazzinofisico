using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaHandling : DaBase
    {
        public DaHandling(DSWarMan.wh_handlingDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DaHandling(DSWarMan.view_handlingDataTable dt)
        {
            this.dt = dt;
        }

        //
        //public DataTable search(Int32 idWarehouseMaster, String strMaterialCode, DateTime dtFrom, DateTime dtTo, Int32 id)
        public DSWarMan.view_handlingDataTable search(Int32 idWarehouseMaster, String strMaterialCode, DateTime dtFrom, DateTime dtTo, Int32 id)
        {
            try
            {
                _conn.Open();
                //DataTable dtHandling = new DataTable("Handling");
                DSWarMan.view_handlingDataTable dtHandling = new DSWarMan.view_handlingDataTable();

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "wh_handling.id as id, " +
                    "wh_handling.datehandling AS datehandling, " +
                    "wh_handling.idmaterial AS idmaterial, " +
                    "wh_handling.typehandling AS typehandling, " +
                    "wh_handling.idwarehouse as idwarehouse, " +
                    "wh_handling.idlocation as idlocation, " +
                    "wh_handling.quantity as quantity, " +
                    "wh_material_master.cdmaterial as cdmaterial," +
                    "wh_material_master.dsmaterial as dsmaterial," +
                    "wh_location_master.cdlocation as cdlocation, " +
                    "wh_warehouse_master.cdwarehousemaster as cdwarehousemaster, " +
                    "wh_warehouse_master.dswarehousemaster as dswarehousemaster " +
                    " FROM wh_handling " +
                    " INNER JOIN wh_material_master " +
                    " ON wh_handling.idmaterial = wh_material_master.id " +
                    " INNER JOIN wh_location_master " +
                    " ON wh_handling.idlocation = wh_location_master.id " +
                    " INNER JOIN wh_warehouse_master " +
                    " ON wh_handling.idwarehouse = wh_warehouse_master.id ";

                String strFilter = "WHERE wh_handling.deleted <> 1";
                // Magazzino
                if (idWarehouseMaster > 0)
                {
                    strFilter += " AND wh_handling.idwarehouse = " + idWarehouseMaster;
                }
                // Materiale
                if (strMaterialCode != String.Empty)
                {
                    strMaterialCode = strMaterialCode.Replace('*', '%');
                    strFilter += " AND wh_material_master.cdmaterial LIKE '" + strMaterialCode + "'";
                }
                // Date inizio e fine
                if (dtFrom > DateTime.MinValue)
                {
                    strFilter += " AND wh_handling.datehandling >= '" + dtFrom.ToString("yyyy-MM-dd") + "'";
                }
                if (dtTo > DateTime.MinValue)
                {
                    strFilter += " AND wh_handling.datehandling <= '" + dtTo.ToString("yyyy-MM-dd") + "'";
                }
                // id
                if (id > 0)
                {
                    strFilter += " AND wh_handling.id = " + id;
                }

                _SelectCmd.CommandText += strFilter;

                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtHandling);

                return dtHandling;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
