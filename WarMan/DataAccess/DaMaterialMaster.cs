using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaMaterialMaster : DaBase
    {
        public DaMaterialMaster(DSWarMan.wh_material_masterDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DataTable search(String strCode, String strDescription, Int32 intIdType)
        {
            try
            {
                _conn.Open();
                DataTable dtMaterialMaster = new DataTable("MaterialMaster");

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "wh_material_master.id as id, " +
                    "wh_material_master.cdmaterial AS cdmaterial, " +
                    "wh_material_master.dsmaterial as dsmaterial, " +
                    "wh_material_master.idmaterialtype as idmaterialtype, " +
                    "wh_material_master.iduom as iduom, " +
                    "wh_material_master.deleted as deleted, " +
                    "wh_material_types.dsmaterialtype as dsmaterialtype" +
                    " FROM wh_material_master " +
                    " INNER JOIN wh_material_types " +
                    " ON wh_material_master.idmaterialtype = wh_material_types.id ";

                String strFilter = "WHERE wh_material_master.deleted <> 1";
                if (strCode != String.Empty)
                {
                    strCode = strCode.Replace('*', '%');
                    strFilter += " AND wh_material_master.cdmaterial LIKE '" + strCode + "'";
                }
                if (strDescription != String.Empty)
                {
                    strDescription = strDescription.Replace('*', '%');
                    strFilter += " AND wh_material_master.dsmaterial LIKE '" + strDescription + "'";
                }
                if (intIdType != -1)
                    strFilter += " AND wh_material_master.idmaterialetype = " + intIdType;

                _SelectCmd.CommandText += strFilter;

                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtMaterialMaster);

                return dtMaterialMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
