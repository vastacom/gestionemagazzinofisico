using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaPickingOrder : DaBase
    {
        public DaPickingOrder(DSWarMan.wh_picking_orderDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DataTable search(String strMateriale, Int32 idMagazzino, String strStato)
        {
            try
            {
                _conn.Open();
                DataTable dtPickingOrder = new DataTable("PickingOrder");

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "wh_picking_order.id as id, " +
                    "wh_picking_order.quantity as quantity, " +
                    "wh_picking_order.state as state, " +
                    "wh_picking_order.idmaterial as idmaterial, " +
                    "wh_picking_order.idwarehousemaster as idwarehousemaster, " +
                    "wh_material_master.cdmaterial as cdmaterial, " +
                    "wh_material_master.dsmaterial as dsmaterial, " +
                    "mt_uom.cduom as cduom, " +
                    "wh_warehouse_master.cdwarehousemaster as cdwarehousemaster, " +
                    "wh_warehouse_master.dswarehousemaster as dswarehousemaster " +
                    " FROM wh_picking_order " +
                    " INNER JOIN wh_material_master " +
                    " ON wh_picking_order.idmaterial = wh_material_master.id " +
                    " INNER JOIN wh_warehouse_master " +
                    " ON wh_picking_order.idwarehousemaster = wh_warehouse_master.id " +
                    " INNER JOIN mt_uom " +
                    " ON wh_material_master.iduom = mt_uom.id ";

                String strFilter = "WHERE wh_picking_order.deleted <> 1";
                if (strMateriale != String.Empty)
                {
                    strMateriale = strMateriale.Replace('*', '%');
                    strFilter += " AND wh_material_master.cdmaterial LIKE '" + strMateriale + "'";
                }
                if (idMagazzino != -1)
                    strFilter += " AND wh_picking_order.idwarehousemaster = " + idMagazzino;
                if (strStato != String.Empty)
                    strFilter += " AND wh_picking_order.state = '" + strStato + "'";                

                _SelectCmd.CommandText += strFilter;

                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtPickingOrder);

                return dtPickingOrder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

    }
}
