﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    class DaLocationMaster : DaBase
    {
        public DaLocationMaster(DSWarMan.wh_location_masterDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DataTable SelectByFilters(Int32 idWarehouseMaster,
        Int32 idLocationType, String cdRoot)
        {
            try
            {
                _conn.Open();
                DataTable dtLocationByFilters = new DataTable("LocationByFilters");

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "lm.id AS id, " +
                    "lm.cdlocation AS cdlocation, " +
                    "lm.cdroot AS cdroot, " +
                    "lt.dslocationtype AS dslocationtype, " +
                    "wm.cdwarehousemaster AS cdwarehousemaster " +
                    "FROM wh_location_master lm " +
                    "INNER JOIN wh_location_types lt ON lm.idlocationtype = lt.id " +
                    "INNER JOIN wh_warehouse_master wm ON lm.idwarehousemaster = wm.id ";

                String strFilter = "lm.deleted <> 1";
                if (idWarehouseMaster != -1)
                    strFilter += " AND lm.idwarehousemaster = " + idWarehouseMaster;
                if (idLocationType != -1)
                    strFilter += " AND lm.idlocationtype = " + idLocationType;
                if (cdRoot != String.Empty)
                    strFilter += " AND lm.cdroot = '" + cdRoot + "'";

                _SelectCmd.CommandText += "WHERE " + strFilter;

                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtLocationByFilters);

                return dtLocationByFilters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
    }
}
