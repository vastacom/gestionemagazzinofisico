using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaStock : DaBase
    {
        public DaStock(DSWarMan.wh_stocksDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DataTable search(Int32 idWarehouseMaster, String strMaterialCode)
        {
            try
            {
                _conn.Open();
                DataTable dtStock = new DataTable("Stock");

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "wh_stocks.id as id, " +
                    "wh_stocks.idmaterial AS idmaterial, " +
                    "wh_stocks.idwarehousemaster as idwarehousemaster, " +
                    "wh_stocks.idlocation as idlocation, " +
                    "wh_stocks.quantity as quantity, " +
                    "wh_stocks.datelastmovement as datelastmovement, " +
                    "wh_material_master.cdmaterial as cdmaterial," +
                    "wh_material_master.dsmaterial as dsmaterial," +
                    "wh_location_master.cdlocation as cdlocation, " +
                    "wh_warehouse_master.cdwarehousemaster as cdwarehousemaster, " +
                    "wh_warehouse_master.dswarehousemaster as dswarehousemaster " +
                    " FROM wh_stocks " +
                    " INNER JOIN wh_material_master " +
                    " ON wh_stocks.idmaterial = wh_material_master.id " +
                    " INNER JOIN wh_location_master " +
                    " ON wh_stocks.idlocation = wh_location_master.id " +
                    " INNER JOIN wh_warehouse_master " +
                    " ON wh_stocks.idwarehousemaster = wh_warehouse_master.id ";

                String strFilter = "WHERE wh_stocks.deleted <> 1";
                if (idWarehouseMaster > 0)
                {
                    strFilter += " AND wh_stocks.idwarehousemaster = " + idWarehouseMaster;
                }
                if (strMaterialCode != String.Empty)
                {
                    strMaterialCode = strMaterialCode.Replace('*', '%');
                    strFilter += " AND wh_material_master.cdmaterial LIKE '" + strMaterialCode + "'";
                }

                _SelectCmd.CommandText += strFilter;

                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtStock);

                return dtStock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
