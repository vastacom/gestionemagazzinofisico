using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Configuration;

namespace WarMan.DataAccess
{
    public class DaBase
    {
        public DataTable dt;
        public long lastInsertRowId;
        protected SQLiteConnection _conn;
        protected SQLiteDataAdapter _da;
        protected SQLiteCommand _SelectCmd;
        protected SQLiteCommand _UpdateCmd;
        protected SQLiteCommand _InsertCmd;
        protected SQLiteCommand _DeleteCmd;

        public DaBase()
        {
            _conn = new SQLiteConnection(ConfigurationManager.ConnectionStrings["SqLiteConn"].ConnectionString);
            _da = new SQLiteDataAdapter();
        }

        public DataTable Select(String strFilter)
        {
            try
            {
                _conn.Open();

                dt.Clear();
                // SelectCmd
                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT * FROM " + dt.TableName + " ";
                if (strFilter != String.Empty)
                    _SelectCmd.CommandText += "WHERE " + strFilter;
                _SelectCmd.CommandText += ";";
                _da.SelectCommand = _SelectCmd;

                _da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public Int32 getNextId()
        {
            try
            {
                _conn.Open();

                dt.Clear();
                // SelectCmd
                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT MAX(id) FROM " + dt.TableName + ";";
                _da.SelectCommand = _SelectCmd;

                _da.Fill(dt);
                return Convert.ToInt32(dt.Rows[0]["id"]) + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public void Update()
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // UpdateCmd
                _UpdateCmd = _conn.CreateCommand();
                _UpdateCmd.CommandText = "UPDATE " + dt.TableName + " SET ";
                String colonne = String.Empty;
                foreach (DataColumn col in dt.Columns)
                {
                    if (dt.PrimaryKey.Contains(col))
                        continue;

                    if (colonne != String.Empty)
                        colonne += ",";

                    colonne += col.ColumnName + " = @" + col.ColumnName;

                    SQLiteParameter param = _UpdateCmd.Parameters.Add("@" + col.ColumnName, GetDbType(col.DataType));
                    param.SourceColumn = col.ColumnName;
                }
                _UpdateCmd.CommandText += colonne + " WHERE ";
                String where = String.Empty;
                foreach (DataColumn col in dt.PrimaryKey)
                {
                    if (where != String.Empty)
                        where += " AND ";

                    where += col.ColumnName + " = @" + col.ColumnName;

                    SQLiteParameter param = _UpdateCmd.Parameters.Add("@" + col.ColumnName, GetDbType(col.DataType));
                    param.SourceColumn = col.ColumnName;
                    param.SourceVersion = DataRowVersion.Original;
                }
                _UpdateCmd.CommandText += where + ";";

                _da.UpdateCommand = _UpdateCmd;

                if (dt.GetChanges(DataRowState.Modified) == null)
                    throw new Exception("Nothing to save");

                int nrow = _da.Update(dt.GetChanges(DataRowState.Modified));
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public void Insert()
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // InsertCmd
                _InsertCmd = _conn.CreateCommand();
                _InsertCmd.CommandText = "INSERT INTO " + dt.TableName + " ( ";
                String colonne = String.Empty;
                foreach (DataColumn col in dt.Columns)
                {
                    if (dt.PrimaryKey.Contains(col) && col.AutoIncrement == true)
                        continue;

                    if (colonne != String.Empty)
                        colonne += ",";

                    colonne += col.ColumnName;
                }
                _InsertCmd.CommandText += colonne + " ) VALUES ( ";
                String valori = String.Empty;
                foreach (DataColumn col in dt.Columns)
                {
                    if (dt.PrimaryKey.Contains(col) && col.AutoIncrement == true)
                        continue;

                    if (valori != String.Empty)
                        valori += ",";

                    valori += "@" + col.ColumnName;

                    SQLiteParameter param = _InsertCmd.Parameters.Add("@" + col.ColumnName, GetDbType(col.DataType));
                    param.SourceColumn = col.ColumnName;
                }
                _InsertCmd.CommandText += valori + " );";
                _da.InsertCommand = _InsertCmd;

                if (dt.GetChanges(DataRowState.Added) == null)
                    throw new Exception("Nothing to save");

                int nrow = _da.Update(dt.GetChanges(DataRowState.Added));
                _tran.Commit();
                lastInsertRowId = _conn.LastInsertRowId;
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public void Delete()
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // DeleteCmd
                _DeleteCmd = _conn.CreateCommand();
                _DeleteCmd.CommandText = "DELETE FROM " + dt.TableName + " WHERE ";
                String where = String.Empty;
                foreach (DataColumn col in dt.PrimaryKey)
                {
                    if (where != String.Empty)
                        where += " AND ";

                    where += col.ColumnName + " = @" + col.ColumnName;

                    SQLiteParameter param = _DeleteCmd.Parameters.Add("@" + col.ColumnName, GetDbType(col.DataType));
                    param.SourceColumn = col.ColumnName;
                    param.SourceVersion = DataRowVersion.Original;
                }
                _DeleteCmd.CommandText += where + ";";

                _da.DeleteCommand = _DeleteCmd;
                int nrow = _da.Update(dt.GetChanges(DataRowState.Deleted));
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public static DbType GetDbType(Type t)
        {
            var typeMap = new Dictionary<Type, DbType>();

            typeMap[typeof(String)] = DbType.String;
            typeMap[typeof(Int32)] = DbType.Int32;
            typeMap[typeof(Int64)] = DbType.Int64;
            typeMap[typeof(Boolean)] = DbType.Boolean;
            typeMap[typeof(Double)] = DbType.Double;
            typeMap[typeof(Decimal)] = DbType.Decimal;
            typeMap[typeof(DateTime)] = DbType.DateTime;
            
            try
            {
                return typeMap[(t)];
            }
            catch
            {
                return DbType.String;
            }
        }

        //

    }
}
