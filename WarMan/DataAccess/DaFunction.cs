using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaFunction : DaBase
    {
        public DaFunction(DSWarMan.sy_functionsDataTable dt)
        {
            this.dt = dt;
        }

        //

        public DSWarMan.sy_functionsDataTable SelectByRole(Int32 idRole)
        {
            try
            {
                _conn.Open();

                dt.Clear();
                // SelectCmd
                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText =
                    "SELECT * FROM sy_functions" +
                    " INNER JOIN sy_roles_functions" +
                    " ON sy_roles_functions.idfunction = sy_functions.id" +
                    " WHERE sy_roles_functions.idrole = " + idRole.ToString() + " AND sy_functions.deleted <> 1" +
                    " ORDER BY CAST(sy_functions.var5 as unsigned) ASC, CAST(sy_functions.var3 as unsigned) ASC;";
                
                _da.SelectCommand = _SelectCmd;

                _da.Fill(dt);
                return (DSWarMan.sy_functionsDataTable)dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
