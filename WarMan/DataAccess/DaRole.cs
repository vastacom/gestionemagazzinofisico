using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaRole : DaBase
    {
        public DaRole(DSWarMan.sy_rolesDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DaRole(DSWarMan.sy_roles_functionsDataTable dt)
        {
            this.dt = dt;
        }

        // 
        public DataTable SelectFunctionsByRole(Int32 idRole)
        {
            try
            {
                _conn.Open();
                DataTable dtFunctionsByRole = new DataTable("FunctionsByRole");
                
                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "CASE WHEN sy_roles_functions.id IS NULL " +
                    "THEN false ELSE true END AS enabled, " +
                    "sy_functions.id as idfunction, " +
                    "sy_functions.dsfunction AS dsfunction, " +
                    "sy_roles_functions.id as id, " +
                    "sy_roles_functions.dtstart as dtstart, " +
                    "sy_roles_functions.dtstop as dtstop" +
                    " FROM sy_functions " +
                    " LEFT JOIN sy_roles_functions" +
                    " ON ( sy_functions.id = sy_roles_functions.idfunction AND " +
                    " sy_roles_functions.idrole = " + idRole.ToString() +
                    " );";
                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtFunctionsByRole);
                
                return dtFunctionsByRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //

        public void DeleteFunctionByRole(Int32 idRole)
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // Delete
                _DeleteCmd = _conn.CreateCommand();
                _DeleteCmd.CommandText = "DELETE FROM sy_roles_functions WHERE idrole = " + idRole.ToString();
                _DeleteCmd.ExecuteNonQuery();
            
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
