using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace WarMan.DataAccess
{
    public class DaUser : DaBase
    {
        public DaUser(DSWarMan.sy_usersDataTable dt)
        {
            this.dt = dt;
        }

        //
        public DaUser(DSWarMan.sy_users_rolesDataTable dt)
        {
            this.dt = dt;
        }

        // 
        public DaUser(DSWarMan.sy_groups_usersDataTable dt)
        {
            this.dt = dt;
        }

        // 
        public DataTable SelectRolesByUser(Int32 idUser)
        {
            try
            {
                _conn.Open();
                DataTable dtRolesByUser = new DataTable("RolesByUser");
                
                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "CASE WHEN sy_users_roles.id IS NULL " +
                    "THEN false ELSE true END AS enabled, " +
                    "sy_roles.id as idrole, " +
                    "sy_roles.dsrole AS dsrole, " +
                    "sy_users_roles.id as id, " +
                    "sy_users_roles.dtstart as dtstart, " +
                    "sy_users_roles.dtstop as dtstop" +
                    " FROM sy_roles " +
                    " LEFT JOIN sy_users_roles" +
                    " ON ( sy_roles.id = sy_users_roles.idrole AND " +
                    " sy_users_roles.iduser = " + idUser.ToString() + ")" +
                    " WHERE sy_roles.deleted <> 1;";
                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtRolesByUser);
                
                return dtRolesByUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //

        public void DeleteRolesByUser(Int32 idUser)
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // Delete
                _DeleteCmd = _conn.CreateCommand();
                _DeleteCmd.CommandText = "DELETE FROM sy_users_roles WHERE iduser = " + idUser.ToString();
                _DeleteCmd.ExecuteNonQuery();
            
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public DataTable SelectGroupsByUser(Int32 idUser)
        {
            try
            {
                _conn.Open();
                DataTable dtGroupsByUser = new DataTable("GroupsByUser");

                _SelectCmd = _conn.CreateCommand();
                _SelectCmd.CommandText = "SELECT " +
                    "CASE WHEN sy_groups_users.id IS NULL " +
                    "THEN false ELSE true END AS enabled, " +
                    "sy_groups.id as idgroup, " +
                    "sy_groups.dsgroup AS dsgroup, " +
                    "sy_groups_users.id as id, " +
                    "sy_groups_users.dtstart as dtstart, " +
                    "sy_groups_users.dtstop as dtstop" +
                    " FROM sy_groups " +
                    " LEFT JOIN sy_groups_users" +
                    " ON ( sy_groups.id = sy_groups_users.idgroup AND " +
                    " sy_groups_users.iduser = " + idUser.ToString() + ")" +
                    " WHERE sy_groups.deleted <> 1;";
                _da.SelectCommand = _SelectCmd;
                _da.Fill(dtGroupsByUser);

                return dtGroupsByUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
        public void DeleteGroupsByUser(Int32 idUser)
        {
            _conn.Open();
            SQLiteTransaction _tran = _conn.BeginTransaction();

            try
            {
                // Delete
                _DeleteCmd = _conn.CreateCommand();
                _DeleteCmd.CommandText = "DELETE FROM sy_groups_users WHERE iduser = " + idUser.ToString();
                _DeleteCmd.ExecuteNonQuery();

                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw ex;
            }
            finally
            {
                _conn.Close();
                _da.Dispose();
            }
        }

        //
    }
}
