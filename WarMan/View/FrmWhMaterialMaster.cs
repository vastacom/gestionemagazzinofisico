﻿using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmWhMaterialMaster : Form
    {
        BusMaterialType busMaterialType;
        BusMaterialMaster busMaterialMaster;

        public FrmWhMaterialMaster()
        {
            InitializeComponent();

            // Riempi la combo
            busMaterialType = new BusMaterialType();
            DataTable dtCmbTipo = busMaterialType.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbTipo.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dsmaterialtype"] = "<Select>";
            dtCmbTipo.Rows.InsertAt(rowEmpty, 0);
            // nella combo
            cmbTipo.DataSource = dtCmbTipo;
            cmbTipo.ValueMember = "id";
            cmbTipo.DisplayMember = "dsmaterialtype";
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.code = txtCode.Text;
            busMaterialMaster.description = txtDescrizione.Text;
            busMaterialMaster.idType = Convert.ToInt32(cmbTipo.SelectedValue);

            DataTable dtMaterialMaster = busMaterialMaster.search();
            dgvMateriali.AutoGenerateColumns = false;
            dgvMateriali.DataSource = dtMaterialMaster;
        }

        private void btnCrea_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text == String.Empty)
                    throw new Exception("Inserire un codice materiale.");

                // Check if material code exists
                busMaterialMaster = new BusMaterialMaster();
                busMaterialMaster.code = txtCode.Text;
                if (busMaterialMaster.checkMaterialExists())
                    throw new Exception("Codice Materiale già presente.");

                FrmWhMaterialCreate frmMaterialCreate = new FrmWhMaterialCreate(busMaterialMaster, true);
                frmMaterialCreate.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Attenzione!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dgvMateriali_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow selectedRow = ((DataGridView) sender).SelectedRows[0];


            busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.id = Convert.ToInt32(selectedRow.Cells["id"].Value);

            FrmWhMaterialCreate frmMaterialCreate = new FrmWhMaterialCreate(busMaterialMaster, false);
            frmMaterialCreate.Show();
        }
    }
}
