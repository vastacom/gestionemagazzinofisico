using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmWhLocationTypes : FrmMasterTable
    {
        BusLocationType busLocationType;

        public FrmWhLocationTypes()
        {
            InitializeComponent();
            busLocationType = new BusLocationType();

            dataPropCode = "cdlocationtype";
            dataPropDescription = "dslocationtype";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busLocationType.getList());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busLocationType.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busLocationType.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
