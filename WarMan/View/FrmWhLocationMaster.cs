﻿using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhLocationMaster : Form
    {
        BusLocationMaster busLocationMaster;
        BusWarehouseMaster busWarehouseMaster;
        BusLocationType busLocationType;
        BindingSource bs;

        public FrmWhLocationMaster()
        {
            InitializeComponent();
            busLocationMaster = new BusLocationMaster();
            //
            dgvLocation.AutoGenerateColumns = false;
            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Select>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbWarehouse.DataSource = dtCmbWarehouse;
            cmbWarehouse.ValueMember = "id";
            cmbWarehouse.DisplayMember = "dswarehousemaster";
            //
            busLocationType = new BusLocationType();
            DataTable dtCmbLocationType = busLocationType.getList();
            // Add Empty
            rowEmpty = dtCmbLocationType.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dslocationtype"] = "<Select>";
            dtCmbLocationType.Rows.InsertAt(rowEmpty, 0);

            cmbLocationType.DataSource = dtCmbLocationType;
            cmbLocationType.ValueMember = "id";
            cmbLocationType.DisplayMember = "dslocationtype";
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            DataTable dt = busLocationMaster.getListByFilters(Convert.ToInt32(cmbWarehouse.SelectedValue), 
                Convert.ToInt32(cmbLocationType.SelectedValue), txtCodiceUbicazione.Text);
            bs = new BindingSource();
            bs.DataSource = dt;
            dgvLocation.DataSource = bs;
        }

        private void btnGenerazione_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cmbLocationType.SelectedValue) == -1 || Convert.ToInt32(cmbWarehouse.SelectedValue) == -1)
                    throw new Exception("Indicare Magazzino e tipo ubicazione");

                if (txtCodiceUbicazione.Text.Length != 3)
                    throw new Exception("Indicare un codice ubicazione di 3 caratteri");

                if (numPiani.Value <= 0)
                    throw new Exception("Indicare un numero di piani maggiore di 0");

                if (numFile.Value <= 0)
                    throw new Exception("Indicare un numero di file maggiore di 0");

                busLocationMaster.generateLocation(
                    Convert.ToInt32(cmbWarehouse.SelectedValue),
                    Convert.ToInt32(cmbLocationType.SelectedValue),
                    txtCodiceUbicazione.Text,
                    Convert.ToInt32(numPiani.Value),
                    Convert.ToInt32(numFile.Value)
                    );

                MessageBox.Show("Generazione completata!", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Attenzione!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnBarcode_Click(object sender, EventArgs e)
        {
            DataTable dt = busLocationMaster.getListByFilters(Convert.ToInt32(cmbWarehouse.SelectedValue),
                Convert.ToInt32(cmbLocationType.SelectedValue), txtCodiceUbicazione.Text);

            FrmLocationsBarCode frmLocationsBarCode = new FrmLocationsBarCode(dt);
            frmLocationsBarCode.ShowDialog();
        }
    }
}
