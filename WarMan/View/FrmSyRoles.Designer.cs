namespace WarMan.View
{
    partial class FrmSyRoles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblKey = new System.Windows.Forms.Label();
            this.dgvListFunctions = new System.Windows.Forms.DataGridView();
            this.enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dsfunction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFunzioni = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListFunctions)).BeginInit();
            this.SuspendLayout();
            // 
            // txtKey
            // 
            this.txtKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKey.Location = new System.Drawing.Point(580, 119);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(148, 20);
            this.txtKey.TabIndex = 15;
            // 
            // lblKey
            // 
            this.lblKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKey.AutoSize = true;
            this.lblKey.Location = new System.Drawing.Point(432, 126);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(25, 13);
            this.lblKey.TabIndex = 14;
            this.lblKey.Text = "Key";
            // 
            // dgvListFunctions
            // 
            this.dgvListFunctions.AllowUserToAddRows = false;
            this.dgvListFunctions.AllowUserToDeleteRows = false;
            this.dgvListFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvListFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListFunctions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.enabled,
            this.dsfunction,
            this.dtstart,
            this.dtstop});
            this.dgvListFunctions.Location = new System.Drawing.Point(405, 191);
            this.dgvListFunctions.MultiSelect = false;
            this.dgvListFunctions.Name = "dgvListFunctions";
            this.dgvListFunctions.Size = new System.Drawing.Size(383, 170);
            this.dgvListFunctions.TabIndex = 16;
            // 
            // enabled
            // 
            this.enabled.HeaderText = "Enabled";
            this.enabled.Name = "enabled";
            this.enabled.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.enabled.Width = 50;
            // 
            // dsfunction
            // 
            this.dsfunction.HeaderText = "Function";
            this.dsfunction.Name = "dsfunction";
            this.dsfunction.ReadOnly = true;
            this.dsfunction.Width = 120;
            // 
            // dtstart
            // 
            this.dtstart.HeaderText = "Date Start";
            this.dtstart.Name = "dtstart";
            this.dtstart.Width = 80;
            // 
            // dtstop
            // 
            this.dtstop.HeaderText = "Date Stop";
            this.dtstop.Name = "dtstop";
            this.dtstop.Width = 80;
            // 
            // lblFunzioni
            // 
            this.lblFunzioni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFunzioni.AutoSize = true;
            this.lblFunzioni.Location = new System.Drawing.Point(402, 175);
            this.lblFunzioni.Name = "lblFunzioni";
            this.lblFunzioni.Size = new System.Drawing.Size(86, 13);
            this.lblFunzioni.TabIndex = 17;
            this.lblFunzioni.Text = "Funzioni Abilitate";
            // 
            // FrmSyRoles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblFunzioni);
            this.Controls.Add(this.dgvListFunctions);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.lblKey);
            this.Name = "FrmSyRoles";
            this.Text = "Ruoli";
            this.Controls.SetChildIndex(this.lblCode, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.txtDescription, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnInsert, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.lblKey, 0);
            this.Controls.SetChildIndex(this.txtKey, 0);
            this.Controls.SetChildIndex(this.dgvListFunctions, 0);
            this.Controls.SetChildIndex(this.lblFunzioni, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListFunctions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.DataGridView dgvListFunctions;
        private System.Windows.Forms.Label lblFunzioni;
        private System.Windows.Forms.DataGridViewCheckBoxColumn enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsfunction;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstart;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstop;
    }
}