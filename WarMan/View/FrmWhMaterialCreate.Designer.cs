﻿
namespace WarMan.View
{
    partial class FrmWhMaterialCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMaterialCode = new System.Windows.Forms.Label();
            this.txtMaterialCode = new System.Windows.Forms.TextBox();
            this.txtDescrizione = new System.Windows.Forms.TextBox();
            this.lblDescrizione = new System.Windows.Forms.Label();
            this.lblUdm = new System.Windows.Forms.Label();
            this.lblTipo = new System.Windows.Forms.Label();
            this.cmbMaterialType = new System.Windows.Forms.ComboBox();
            this.cmbUdm = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMaterialCode
            // 
            this.lblMaterialCode.AutoSize = true;
            this.lblMaterialCode.Location = new System.Drawing.Point(12, 36);
            this.lblMaterialCode.Name = "lblMaterialCode";
            this.lblMaterialCode.Size = new System.Drawing.Size(86, 13);
            this.lblMaterialCode.TabIndex = 0;
            this.lblMaterialCode.Text = "Codice Materiale";
            // 
            // txtMaterialCode
            // 
            this.txtMaterialCode.Location = new System.Drawing.Point(157, 33);
            this.txtMaterialCode.Name = "txtMaterialCode";
            this.txtMaterialCode.ReadOnly = true;
            this.txtMaterialCode.Size = new System.Drawing.Size(171, 20);
            this.txtMaterialCode.TabIndex = 1;
            // 
            // txtDescrizione
            // 
            this.txtDescrizione.Location = new System.Drawing.Point(157, 69);
            this.txtDescrizione.Name = "txtDescrizione";
            this.txtDescrizione.Size = new System.Drawing.Size(285, 20);
            this.txtDescrizione.TabIndex = 3;
            // 
            // lblDescrizione
            // 
            this.lblDescrizione.AutoSize = true;
            this.lblDescrizione.Location = new System.Drawing.Point(12, 72);
            this.lblDescrizione.Name = "lblDescrizione";
            this.lblDescrizione.Size = new System.Drawing.Size(62, 13);
            this.lblDescrizione.TabIndex = 2;
            this.lblDescrizione.Text = "Descrizione";
            // 
            // lblUdm
            // 
            this.lblUdm.AutoSize = true;
            this.lblUdm.Location = new System.Drawing.Point(12, 148);
            this.lblUdm.Name = "lblUdm";
            this.lblUdm.Size = new System.Drawing.Size(30, 13);
            this.lblUdm.TabIndex = 4;
            this.lblUdm.Text = "UdM";
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(12, 110);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(74, 13);
            this.lblTipo.TabIndex = 5;
            this.lblTipo.Text = "Tipo Materiale";
            // 
            // cmbMaterialType
            // 
            this.cmbMaterialType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaterialType.FormattingEnabled = true;
            this.cmbMaterialType.Location = new System.Drawing.Point(157, 107);
            this.cmbMaterialType.Name = "cmbMaterialType";
            this.cmbMaterialType.Size = new System.Drawing.Size(171, 21);
            this.cmbMaterialType.TabIndex = 6;
            // 
            // cmbUdm
            // 
            this.cmbUdm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUdm.FormattingEnabled = true;
            this.cmbUdm.Location = new System.Drawing.Point(157, 145);
            this.cmbUdm.Name = "cmbUdm";
            this.cmbUdm.Size = new System.Drawing.Size(171, 21);
            this.cmbUdm.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(12, 375);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(136, 72);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmWhMaterialCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 459);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbUdm);
            this.Controls.Add(this.cmbMaterialType);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.lblUdm);
            this.Controls.Add(this.txtDescrizione);
            this.Controls.Add(this.lblDescrizione);
            this.Controls.Add(this.txtMaterialCode);
            this.Controls.Add(this.lblMaterialCode);
            this.Name = "FrmWhMaterialCreate";
            this.Text = "Creazione Anagrafica Materiale";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMaterialCode;
        private System.Windows.Forms.TextBox txtMaterialCode;
        private System.Windows.Forms.TextBox txtDescrizione;
        private System.Windows.Forms.Label lblDescrizione;
        private System.Windows.Forms.Label lblUdm;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox cmbMaterialType;
        private System.Windows.Forms.ComboBox cmbUdm;
        private System.Windows.Forms.Button btnSave;
    }
}