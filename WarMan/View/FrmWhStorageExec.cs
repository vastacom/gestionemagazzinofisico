﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhStorageExec : Form
    {
        BusStorageOrder busStorageOrder;

        public FrmWhStorageExec()
        {
            InitializeComponent();
        }

        public FrmWhStorageExec(Int32 idStorageOrder)
        {
            InitializeComponent();

            busStorageOrder = new BusStorageOrder();
            busStorageOrder.id = idStorageOrder;
            busStorageOrder.getDetailsById();

            txtNumOrdine.Text = busStorageOrder.id.ToString();
            txtQtaOrdine.Text = busStorageOrder.quantita.ToString();
            nQuantita.Value = busStorageOrder.quantita - busStorageOrder.qtaEseguita;
            //
            BusMaterialMaster busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.id = busStorageOrder.idMateriale;
            busMaterialMaster.getDetailsById();
            txtCodiceMateriale.Text = busMaterialMaster.code;
            txtDescrMateriale.Text = busMaterialMaster.description;
            //
            BusWarehouseMaster busWarehouseMaster = new BusWarehouseMaster();
            busWarehouseMaster.id = busStorageOrder.idMagazzino;
            busWarehouseMaster.getDetailsById();
            txtCodMagazzino.Text = busWarehouseMaster.code;
            txtDescMagazzino.Text = busWarehouseMaster.description;
            //
            // Ubicazioni
            BusLocationMaster busLocationMaster = new BusLocationMaster();
            DataTable dt = busLocationMaster.getListByFilters(busWarehouseMaster.id, -1, "");
            dgvLocation.AutoGenerateColumns = false;
            dgvLocation.DataSource = dt;
        }

        private void bntEsegui_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLocation.SelectedRows.Count <= 0)
                    throw new Exception("Occorre selezionare un'ubicazione di deposito.");
                if (nQuantita.Value <= 0)
                    throw new Exception("La quantità depositata deve essere > 0.");
                if (nQuantita.Value > (busStorageOrder.quantita - busStorageOrder.qtaEseguita))
                    throw new Exception("La quantità depositata è maggiore della quantità disponibile dell'ordine.");

                busStorageOrder.qtaEseguita += nQuantita.Value;
                busStorageOrder.execute(Convert.ToInt32(dgvLocation.SelectedRows[0].Cells["id"].Value), nQuantita.Value);

                MessageBox.Show("Deposito Eseguito!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bntEsegui.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
    }
}
