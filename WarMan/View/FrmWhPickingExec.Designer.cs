﻿
namespace WarMan.View
{
    partial class FrmWhPickingExec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumOrdine = new System.Windows.Forms.Label();
            this.txtNumOrdine = new System.Windows.Forms.TextBox();
            this.txtCodiceMateriale = new System.Windows.Forms.TextBox();
            this.lblMateriale = new System.Windows.Forms.Label();
            this.txtDescrMateriale = new System.Windows.Forms.TextBox();
            this.lblQtaDeposito = new System.Windows.Forms.Label();
            this.nQuantita = new System.Windows.Forms.NumericUpDown();
            this.lblMagazzino = new System.Windows.Forms.Label();
            this.txtDescMagazzino = new System.Windows.Forms.TextBox();
            this.txtCodMagazzino = new System.Windows.Forms.TextBox();
            this.lblUbicazione = new System.Windows.Forms.Label();
            this.bntEsegui = new System.Windows.Forms.Button();
            this.dgvLocation = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdlocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblQtaOrdine = new System.Windows.Forms.Label();
            this.txtQtaOrdine = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nQuantita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumOrdine
            // 
            this.lblNumOrdine.AutoSize = true;
            this.lblNumOrdine.Location = new System.Drawing.Point(9, 24);
            this.lblNumOrdine.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumOrdine.Name = "lblNumOrdine";
            this.lblNumOrdine.Size = new System.Drawing.Size(66, 13);
            this.lblNumOrdine.TabIndex = 0;
            this.lblNumOrdine.Text = "Num. Ordine";
            // 
            // txtNumOrdine
            // 
            this.txtNumOrdine.Location = new System.Drawing.Point(100, 21);
            this.txtNumOrdine.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNumOrdine.Name = "txtNumOrdine";
            this.txtNumOrdine.ReadOnly = true;
            this.txtNumOrdine.Size = new System.Drawing.Size(102, 20);
            this.txtNumOrdine.TabIndex = 1;
            // 
            // txtCodiceMateriale
            // 
            this.txtCodiceMateriale.Location = new System.Drawing.Point(100, 57);
            this.txtCodiceMateriale.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCodiceMateriale.Name = "txtCodiceMateriale";
            this.txtCodiceMateriale.ReadOnly = true;
            this.txtCodiceMateriale.Size = new System.Drawing.Size(102, 20);
            this.txtCodiceMateriale.TabIndex = 3;
            // 
            // lblMateriale
            // 
            this.lblMateriale.AutoSize = true;
            this.lblMateriale.Location = new System.Drawing.Point(9, 59);
            this.lblMateriale.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMateriale.Name = "lblMateriale";
            this.lblMateriale.Size = new System.Drawing.Size(50, 13);
            this.lblMateriale.TabIndex = 2;
            this.lblMateriale.Text = "Materiale";
            // 
            // txtDescrMateriale
            // 
            this.txtDescrMateriale.Location = new System.Drawing.Point(212, 57);
            this.txtDescrMateriale.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDescrMateriale.Name = "txtDescrMateriale";
            this.txtDescrMateriale.ReadOnly = true;
            this.txtDescrMateriale.Size = new System.Drawing.Size(343, 20);
            this.txtDescrMateriale.TabIndex = 4;
            // 
            // lblQtaDeposito
            // 
            this.lblQtaDeposito.AutoSize = true;
            this.lblQtaDeposito.Location = new System.Drawing.Point(9, 93);
            this.lblQtaDeposito.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQtaDeposito.Name = "lblQtaDeposito";
            this.lblQtaDeposito.Size = new System.Drawing.Size(72, 13);
            this.lblQtaDeposito.TabIndex = 5;
            this.lblQtaDeposito.Text = "Q.tà Deposito";
            // 
            // nQuantita
            // 
            this.nQuantita.Location = new System.Drawing.Point(100, 92);
            this.nQuantita.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nQuantita.Name = "nQuantita";
            this.nQuantita.Size = new System.Drawing.Size(90, 20);
            this.nQuantita.TabIndex = 6;
            // 
            // lblMagazzino
            // 
            this.lblMagazzino.AutoSize = true;
            this.lblMagazzino.Location = new System.Drawing.Point(9, 128);
            this.lblMagazzino.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMagazzino.Name = "lblMagazzino";
            this.lblMagazzino.Size = new System.Drawing.Size(58, 13);
            this.lblMagazzino.TabIndex = 7;
            this.lblMagazzino.Text = "Magazzino";
            // 
            // txtDescMagazzino
            // 
            this.txtDescMagazzino.Location = new System.Drawing.Point(212, 126);
            this.txtDescMagazzino.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDescMagazzino.Name = "txtDescMagazzino";
            this.txtDescMagazzino.ReadOnly = true;
            this.txtDescMagazzino.Size = new System.Drawing.Size(343, 20);
            this.txtDescMagazzino.TabIndex = 9;
            // 
            // txtCodMagazzino
            // 
            this.txtCodMagazzino.Location = new System.Drawing.Point(100, 126);
            this.txtCodMagazzino.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCodMagazzino.Name = "txtCodMagazzino";
            this.txtCodMagazzino.ReadOnly = true;
            this.txtCodMagazzino.Size = new System.Drawing.Size(102, 20);
            this.txtCodMagazzino.TabIndex = 8;
            // 
            // lblUbicazione
            // 
            this.lblUbicazione.AutoSize = true;
            this.lblUbicazione.Location = new System.Drawing.Point(9, 166);
            this.lblUbicazione.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUbicazione.Name = "lblUbicazione";
            this.lblUbicazione.Size = new System.Drawing.Size(60, 13);
            this.lblUbicazione.TabIndex = 11;
            this.lblUbicazione.Text = "Ubicazione";
            // 
            // bntEsegui
            // 
            this.bntEsegui.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntEsegui.Location = new System.Drawing.Point(11, 406);
            this.bntEsegui.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bntEsegui.Name = "bntEsegui";
            this.bntEsegui.Size = new System.Drawing.Size(118, 66);
            this.bntEsegui.TabIndex = 12;
            this.bntEsegui.Text = "ESEGUI";
            this.bntEsegui.UseVisualStyleBackColor = true;
            this.bntEsegui.Click += new System.EventHandler(this.bntEsegui_Click);
            // 
            // dgvLocation
            // 
            this.dgvLocation.AllowUserToAddRows = false;
            this.dgvLocation.AllowUserToDeleteRows = false;
            this.dgvLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.cdlocation,
            this.locationType});
            this.dgvLocation.Location = new System.Drawing.Point(100, 166);
            this.dgvLocation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvLocation.MultiSelect = false;
            this.dgvLocation.Name = "dgvLocation";
            this.dgvLocation.ReadOnly = true;
            this.dgvLocation.RowHeadersWidth = 51;
            this.dgvLocation.RowTemplate.Height = 24;
            this.dgvLocation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLocation.Size = new System.Drawing.Size(454, 228);
            this.dgvLocation.TabIndex = 13;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // cdlocation
            // 
            this.cdlocation.DataPropertyName = "cdlocation";
            this.cdlocation.HeaderText = "Codice Ubicazione";
            this.cdlocation.MinimumWidth = 6;
            this.cdlocation.Name = "cdlocation";
            this.cdlocation.ReadOnly = true;
            this.cdlocation.Width = 180;
            // 
            // locationType
            // 
            this.locationType.DataPropertyName = "dslocationtype";
            this.locationType.HeaderText = "Tipo Ubicazione";
            this.locationType.MinimumWidth = 6;
            this.locationType.Name = "locationType";
            this.locationType.ReadOnly = true;
            this.locationType.Width = 180;
            // 
            // lblQtaOrdine
            // 
            this.lblQtaOrdine.AutoSize = true;
            this.lblQtaOrdine.Location = new System.Drawing.Point(209, 96);
            this.lblQtaOrdine.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQtaOrdine.Name = "lblQtaOrdine";
            this.lblQtaOrdine.Size = new System.Drawing.Size(61, 13);
            this.lblQtaOrdine.TabIndex = 14;
            this.lblQtaOrdine.Text = "Q.tà Ordine";
            // 
            // txtQtaOrdine
            // 
            this.txtQtaOrdine.Location = new System.Drawing.Point(286, 93);
            this.txtQtaOrdine.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtQtaOrdine.Name = "txtQtaOrdine";
            this.txtQtaOrdine.ReadOnly = true;
            this.txtQtaOrdine.Size = new System.Drawing.Size(80, 20);
            this.txtQtaOrdine.TabIndex = 15;
            // 
            // FrmWhPickingExec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 482);
            this.Controls.Add(this.txtQtaOrdine);
            this.Controls.Add(this.lblQtaOrdine);
            this.Controls.Add(this.dgvLocation);
            this.Controls.Add(this.bntEsegui);
            this.Controls.Add(this.lblUbicazione);
            this.Controls.Add(this.txtDescMagazzino);
            this.Controls.Add(this.txtCodMagazzino);
            this.Controls.Add(this.lblMagazzino);
            this.Controls.Add(this.nQuantita);
            this.Controls.Add(this.lblQtaDeposito);
            this.Controls.Add(this.txtDescrMateriale);
            this.Controls.Add(this.txtCodiceMateriale);
            this.Controls.Add(this.lblMateriale);
            this.Controls.Add(this.txtNumOrdine);
            this.Controls.Add(this.lblNumOrdine);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmWhPickingExec";
            this.Text = "Esecuzione Prelievo";
            ((System.ComponentModel.ISupportInitialize)(this.nQuantita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNumOrdine;
        private System.Windows.Forms.TextBox txtNumOrdine;
        private System.Windows.Forms.TextBox txtCodiceMateriale;
        private System.Windows.Forms.Label lblMateriale;
        private System.Windows.Forms.TextBox txtDescrMateriale;
        private System.Windows.Forms.Label lblQtaDeposito;
        private System.Windows.Forms.NumericUpDown nQuantita;
        private System.Windows.Forms.Label lblMagazzino;
        private System.Windows.Forms.TextBox txtDescMagazzino;
        private System.Windows.Forms.TextBox txtCodMagazzino;
        private System.Windows.Forms.Label lblUbicazione;
        private System.Windows.Forms.Button bntEsegui;
        private System.Windows.Forms.DataGridView dgvLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdlocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationType;
        private System.Windows.Forms.Label lblQtaOrdine;
        private System.Windows.Forms.TextBox txtQtaOrdine;
    }
}