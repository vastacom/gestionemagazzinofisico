﻿
namespace WarMan.View
{
    partial class FrmWhStorageSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvStorageOrder = new System.Windows.Forms.DataGridView();
            this.nOrdine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdmateriale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsMateriale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdmagazzino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsMagazzino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMateriale = new System.Windows.Forms.Label();
            this.txtMateriale = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnEsegui = new System.Windows.Forms.Button();
            this.lblMagazzino = new System.Windows.Forms.Label();
            this.cmbMagazzino = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvStorageOrder
            // 
            this.dgvStorageOrder.AllowUserToAddRows = false;
            this.dgvStorageOrder.AllowUserToDeleteRows = false;
            this.dgvStorageOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvStorageOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStorageOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nOrdine,
            this.cdmateriale,
            this.dsMateriale,
            this.quantita,
            this.uom,
            this.cdmagazzino,
            this.dsMagazzino,
            this.stato});
            this.dgvStorageOrder.Location = new System.Drawing.Point(12, 179);
            this.dgvStorageOrder.Name = "dgvStorageOrder";
            this.dgvStorageOrder.ReadOnly = true;
            this.dgvStorageOrder.RowHeadersWidth = 51;
            this.dgvStorageOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStorageOrder.Size = new System.Drawing.Size(872, 374);
            this.dgvStorageOrder.TabIndex = 0;
            // 
            // nOrdine
            // 
            this.nOrdine.DataPropertyName = "id";
            this.nOrdine.HeaderText = "Num. Ordine";
            this.nOrdine.MinimumWidth = 6;
            this.nOrdine.Name = "nOrdine";
            this.nOrdine.ReadOnly = true;
            this.nOrdine.Width = 120;
            // 
            // cdmateriale
            // 
            this.cdmateriale.DataPropertyName = "cdMaterial";
            this.cdmateriale.HeaderText = "Cod. Materiale";
            this.cdmateriale.MinimumWidth = 6;
            this.cdmateriale.Name = "cdmateriale";
            this.cdmateriale.ReadOnly = true;
            this.cdmateriale.Width = 140;
            // 
            // dsMateriale
            // 
            this.dsMateriale.DataPropertyName = "dsMaterial";
            this.dsMateriale.HeaderText = "Descr. Materiale";
            this.dsMateriale.MinimumWidth = 6;
            this.dsMateriale.Name = "dsMateriale";
            this.dsMateriale.ReadOnly = true;
            this.dsMateriale.Width = 240;
            // 
            // quantita
            // 
            this.quantita.DataPropertyName = "quantity";
            this.quantita.HeaderText = "Q.tà";
            this.quantita.MinimumWidth = 6;
            this.quantita.Name = "quantita";
            this.quantita.ReadOnly = true;
            this.quantita.Width = 90;
            // 
            // uom
            // 
            this.uom.DataPropertyName = "cduom";
            this.uom.HeaderText = "UdM";
            this.uom.MinimumWidth = 6;
            this.uom.Name = "uom";
            this.uom.ReadOnly = true;
            this.uom.Width = 90;
            // 
            // cdmagazzino
            // 
            this.cdmagazzino.DataPropertyName = "cdwarehousemaster";
            this.cdmagazzino.HeaderText = "Cod. Mag.";
            this.cdmagazzino.MinimumWidth = 6;
            this.cdmagazzino.Name = "cdmagazzino";
            this.cdmagazzino.ReadOnly = true;
            this.cdmagazzino.Width = 125;
            // 
            // dsMagazzino
            // 
            this.dsMagazzino.DataPropertyName = "dswarehousemaster";
            this.dsMagazzino.HeaderText = "Descr. Magazzino";
            this.dsMagazzino.MinimumWidth = 6;
            this.dsMagazzino.Name = "dsMagazzino";
            this.dsMagazzino.ReadOnly = true;
            this.dsMagazzino.Width = 180;
            // 
            // stato
            // 
            this.stato.DataPropertyName = "state";
            this.stato.HeaderText = "Stato";
            this.stato.MinimumWidth = 6;
            this.stato.Name = "stato";
            this.stato.ReadOnly = true;
            this.stato.Width = 125;
            // 
            // lblMateriale
            // 
            this.lblMateriale.AutoSize = true;
            this.lblMateriale.Location = new System.Drawing.Point(26, 29);
            this.lblMateriale.Name = "lblMateriale";
            this.lblMateriale.Size = new System.Drawing.Size(50, 13);
            this.lblMateriale.TabIndex = 1;
            this.lblMateriale.Text = "Materiale";
            // 
            // txtMateriale
            // 
            this.txtMateriale.Location = new System.Drawing.Point(138, 26);
            this.txtMateriale.Name = "txtMateriale";
            this.txtMateriale.Size = new System.Drawing.Size(121, 20);
            this.txtMateriale.TabIndex = 2;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(724, 18);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(151, 68);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "SEARCH";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnEsegui
            // 
            this.btnEsegui.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEsegui.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEsegui.Location = new System.Drawing.Point(12, 593);
            this.btnEsegui.Name = "btnEsegui";
            this.btnEsegui.Size = new System.Drawing.Size(151, 68);
            this.btnEsegui.TabIndex = 4;
            this.btnEsegui.Text = "ESEGUI";
            this.btnEsegui.UseVisualStyleBackColor = true;
            this.btnEsegui.Click += new System.EventHandler(this.btnEsegui_Click);
            // 
            // lblMagazzino
            // 
            this.lblMagazzino.AutoSize = true;
            this.lblMagazzino.Location = new System.Drawing.Point(26, 68);
            this.lblMagazzino.Name = "lblMagazzino";
            this.lblMagazzino.Size = new System.Drawing.Size(58, 13);
            this.lblMagazzino.TabIndex = 5;
            this.lblMagazzino.Text = "Magazzino";
            // 
            // cmbMagazzino
            // 
            this.cmbMagazzino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMagazzino.FormattingEnabled = true;
            this.cmbMagazzino.Location = new System.Drawing.Point(138, 65);
            this.cmbMagazzino.Name = "cmbMagazzino";
            this.cmbMagazzino.Size = new System.Drawing.Size(121, 21);
            this.cmbMagazzino.TabIndex = 6;
            // 
            // FrmWhStorageSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 673);
            this.Controls.Add(this.cmbMagazzino);
            this.Controls.Add(this.lblMagazzino);
            this.Controls.Add(this.btnEsegui);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtMateriale);
            this.Controls.Add(this.lblMateriale);
            this.Controls.Add(this.dgvStorageOrder);
            this.Name = "FrmWhStorageSelect";
            this.Text = "Ordini di Deposito";
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageOrder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvStorageOrder;
        private System.Windows.Forms.Label lblMateriale;
        private System.Windows.Forms.TextBox txtMateriale;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnEsegui;
        private System.Windows.Forms.Label lblMagazzino;
        private System.Windows.Forms.ComboBox cmbMagazzino;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOrdine;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdmateriale;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsMateriale;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantita;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdmagazzino;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsMagazzino;
        private System.Windows.Forms.DataGridViewTextBoxColumn stato;
    }
}