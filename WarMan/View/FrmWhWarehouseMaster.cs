using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmWhWarehouseMaster : FrmMasterTable
    {
        BusWarehouseMaster busWarehouseMaster;

        public FrmWhWarehouseMaster()
        {
            InitializeComponent();
            busWarehouseMaster = new BusWarehouseMaster();

            dataPropCode = "cdwarehousemaster";
            dataPropDescription = "dswarehousemaster";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busWarehouseMaster.getList());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busWarehouseMaster.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busWarehouseMaster.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
