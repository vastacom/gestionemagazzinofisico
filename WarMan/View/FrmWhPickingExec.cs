﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhPickingExec : Form
    {
        BusPickingOrder busPickingOrder;

        public FrmWhPickingExec()
        {
            InitializeComponent();
        }

        public FrmWhPickingExec(Int32 idPickingOrder)
        {
            InitializeComponent();

            busPickingOrder = new BusPickingOrder();
            busPickingOrder.id = idPickingOrder;
            busPickingOrder.getDetailsById();

            txtNumOrdine.Text = busPickingOrder.id.ToString();
            txtQtaOrdine.Text = busPickingOrder.quantita.ToString();
            nQuantita.Value = busPickingOrder.quantita - busPickingOrder.qtaEseguita;
            //
            BusMaterialMaster busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.id = busPickingOrder.idMateriale;
            busMaterialMaster.getDetailsById();
            txtCodiceMateriale.Text = busMaterialMaster.code;
            txtDescrMateriale.Text = busMaterialMaster.description;
            //
            BusWarehouseMaster busWarehouseMaster = new BusWarehouseMaster();
            busWarehouseMaster.id = busPickingOrder.idMagazzino;
            busWarehouseMaster.getDetailsById();
            txtCodMagazzino.Text = busWarehouseMaster.code;
            txtDescMagazzino.Text = busWarehouseMaster.description;
            //
            // Ubicazioni
            BusLocationMaster busLocationMaster = new BusLocationMaster();
            DataTable dt = busLocationMaster.getListByFilters(busWarehouseMaster.id, -1, "");
            dgvLocation.AutoGenerateColumns = false;
            dgvLocation.DataSource = dt;
        }

        private void bntEsegui_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLocation.SelectedRows.Count <= 0)
                    throw new Exception("Occorre selezionare un'ubicazione di prelievo.");
                if (nQuantita.Value <= 0)
                    throw new Exception("La quantità prelevata deve essere > 0.");
                if (nQuantita.Value > (busPickingOrder.quantita - busPickingOrder.qtaEseguita))
                    throw new Exception("La quantità prelevata è maggiore della quantità disponibile dell'ordine.");

                busPickingOrder.qtaEseguita += nQuantita.Value;
                busPickingOrder.execute(Convert.ToInt32(dgvLocation.SelectedRows[0].Cells["id"].Value), nQuantita.Value);

                MessageBox.Show("Prelievo Eseguito!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bntEsegui.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
    }
}
