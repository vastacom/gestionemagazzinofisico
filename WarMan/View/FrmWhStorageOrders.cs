﻿using System;
using System.Data;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhStorageOrders : Form
    {
        public BusMaterialMaster busMaterialMaster;
        BusWarehouseMaster busWarehouseMaster;
        BusStorageOrder busStorageOrder;
        Boolean _addMode;

        public FrmWhStorageOrders()
        {
            InitializeComponent();

            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Any>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbMagazzino.DataSource = dtCmbWarehouse;
            cmbMagazzino.ValueMember = "id";
            cmbMagazzino.DisplayMember = "dswarehousemaster";
            //
        }

        private void btnSerchMaterial_Click(object sender, EventArgs e)
        {
            FrmHelpMaterial help = new FrmHelpMaterial(txtMateriale.Text);
            help.ShowDialog(this);

            busMaterialMaster.getDetailsById();
            //
            BusUom busUom = new BusUom();
            busUom.id = busMaterialMaster.idUom;
            busUom.getDetailsById();
            //
            txtMateriale.Text = busMaterialMaster.code;
            lblDescrizioneMateriale.Text = busMaterialMaster.description;
            lblUom.Text = busUom.code;
        }

        private void txtMateriale_TextChanged(object sender, EventArgs e)
        {
            lblDescrizioneMateriale.Text = String.Empty;
            lblUom.Text = String.Empty;
        }

        private void txtMateriale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                busMaterialMaster = new BusMaterialMaster();
                busMaterialMaster.code = txtMateriale.Text;
                busMaterialMaster.search();
                //
                BusUom busUom = new BusUom();
                busUom.id = busMaterialMaster.idUom;
                busUom.getDetailsById();
                //
                txtMateriale.Text = busMaterialMaster.code;
                lblDescrizioneMateriale.Text = busMaterialMaster.description;
                lblUom.Text = busUom.code;
            }
        }

        //
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                _addMode = false;
                if (busStorageOrder == null)
                {
                    busStorageOrder = new BusStorageOrder();
                    _addMode = true;
                }
                busStorageOrder.idMateriale = busMaterialMaster.id;
                busStorageOrder.quantita = nQuantita.Value;
                busStorageOrder.idMagazzino = Convert.ToInt32(cmbMagazzino.SelectedValue);
                busStorageOrder.stato = "INSERITO";

                busStorageOrder.save(_addMode);

                txtNumero.Text = busStorageOrder.id.ToString();
                txtState.Text = busStorageOrder.stato;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
