﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmLogin : Form
    {
        BusUser busUser = new BusUser();

        public FrmLogin()
        {
            InitializeComponent();

            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Trim() == String.Empty)
                    throw new Exception("UserName is required");
                if (PasswordTextBox.Text.Trim() == String.Empty)
                    throw new Exception("Password is required");

                //
                User.userName = UsernameTextBox.Text;
                User.password = PasswordTextBox.Text;

                DataTable dtUtente = busUser.getUserByName(User.userName);
                if (dtUtente.Rows.Count == 0)
                    throw new Exception("UserName not found");

                // eventualmente criptare pwd...
                if (Convert.ToString(dtUtente.Rows[0]["password"]) != User.password)
                    throw new Exception("Credentials Wrong!");

                // Se arrivo qui sono autenticato e recupero il ruolo
                busUser.id = Convert.ToInt32(dtUtente.Rows[0]["id"]);
                DataTable dtRoles = busUser.getRolesByUser();
                if (dtRoles.Rows.Count == 0)
                    throw new Exception("User Role not found!");

                User.ruolo = Convert.ToString(dtRoles.Rows[0]["idrole"]);

                User.IsAuthenticated = true;
                this.Close();
            }
            catch (Exception ex)
            {
                User.IsAuthenticated = false;
                MessageBox.Show(ex.Message, "iPMC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!User.IsAuthenticated)
                Application.Exit();
        }

    }
}
