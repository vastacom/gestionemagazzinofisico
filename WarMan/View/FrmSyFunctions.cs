using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmSyFunctions : FrmMasterTable
    {
        BusFunction busFunction;
        protected String dataPropKey;
        protected String dataPropVar1;
        protected String dataPropVar2;
        protected String dataPropVar3;
        protected String dataPropVar4;
        protected String dataPropVar5;

        public FrmSyFunctions()
        {
            InitializeComponent();
            busFunction = new BusFunction();

            dataPropCode = "cdfunction";
            dataPropDescription = "dsfunction";
            dataPropKey = "functionkey";
            dataPropVar1 = "var1";
            dataPropVar2 = "var2";
            dataPropVar3 = "var3";
            dataPropVar4 = "var4";
            dataPropVar5 = "var5";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busFunction.getList());
                txtKey.DataBindings.Add("Text", bs, dataPropKey);
                txtVar1.DataBindings.Add("Text", bs, dataPropVar1);
                txtVar2.DataBindings.Add("Text", bs, dataPropVar2);
                txtVar3.DataBindings.Add("Text", bs, dataPropVar3);
                txtVar4.DataBindings.Add("Text", bs, dataPropVar4);
                txtVar5.DataBindings.Add("Text", bs, dataPropVar5);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busFunction.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busFunction.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
