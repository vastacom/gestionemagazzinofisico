﻿using System;
using System.Data;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmHelpMaterial : Form
    {
        BusMaterialMaster busMaterialMaster;

        public FrmHelpMaterial()
        {
            InitializeComponent();
        }
        //
        public FrmHelpMaterial(String strCodiceMateriale)
        {
            InitializeComponent();

            txtCodice.Text = strCodiceMateriale;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.code = txtCodice.Text;
            busMaterialMaster.description = txtDescrizione.Text;
            busMaterialMaster.idType = -1;

            DataTable dtMaterialMaster = busMaterialMaster.search();
            //dgvResults.AutoGenerateColumns = false;
            dgvResults.DataSource = dtMaterialMaster;
        }

        private void dgvResults_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow selectedRow = ((DataGridView)sender).SelectedRows[0];

            busMaterialMaster = new BusMaterialMaster();
            busMaterialMaster.id = Convert.ToInt32(selectedRow.Cells["id"].Value);

            if (this.Owner.ActiveMdiChild.GetType().Equals(typeof(FrmWhStorageOrders)))
                ((FrmWhStorageOrders)this.Owner.ActiveMdiChild).busMaterialMaster = busMaterialMaster;
            else if (this.Owner.ActiveMdiChild.GetType().Equals(typeof(FrmWhPickingOrders)))
                ((FrmWhPickingOrders)this.Owner.ActiveMdiChild).busMaterialMaster = busMaterialMaster;

            this.Close();
        }
    }
}
