﻿using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmMtNations : FrmMasterTable
    {
        BusNation busNation;
        BusLanguage busLanguage;
        BusCurrency busCurrency;

        public FrmMtNations()
        {
            InitializeComponent();
            busNation = new BusNation();

            // Riempi le combo
            busLanguage = new BusLanguage();
            cmbLanguage.DataSource = busLanguage.getList();
            cmbLanguage.ValueMember = "id";
            cmbLanguage.DisplayMember = "dslanguage";

            busCurrency = new BusCurrency();
            cmbCurrency.DataSource = busCurrency.getList();
            cmbCurrency.ValueMember = "id";
            cmbCurrency.DisplayMember = "dscurrency";

            dataPropCode = "cdnation";
            dataPropDescription = "dsnation";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busNation.getList());
                cmbLanguage.DataBindings.Add("SelectedValue", bs, "idlanguage");
                cmbCurrency.DataBindings.Add("SelectedValue", bs, "idcurrency");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busNation.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busNation.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //

    }
}
