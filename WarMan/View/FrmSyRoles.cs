using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmSyRoles : FrmMasterTable
    {
        BusRole busRole;
        protected String dataPropKey;
        //
        DataTable dtFunctions = new DataTable("dtFunctions");
        BindingSource bsFunctions = new BindingSource();

        public FrmSyRoles()
        {
            InitializeComponent();
            busRole = new BusRole();

            dataPropCode = "cdrole";
            dataPropDescription = "dsrole";
            dataPropKey = "rolekey";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busRole.getList());
                txtKey.DataBindings.Add("Text", bs, dataPropKey);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
        protected override void dgvList_RowValidating(object sender, DataGridViewCellCancelEventArgs e) 
        {
            if (dtFunctions.GetChanges() != null && dgvList.Focused)
            {
                DialogResult result = MessageBox.Show("I dati non salvati andranno persi.\nContinuare?", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        //
        protected override void dgvList_SelectionChanged(object sender, EventArgs e)
        {
            if (addMode) 
                return;
            //
            DataRowView CurrentRow = (DataRowView)bs.Current;
            busRole.id = Convert.ToInt32(CurrentRow.Row["id"]);

            // Functions Enabled
            dgvListFunctions.AutoGenerateColumns = false;
            dtFunctions = busRole.getFunctionsByRole();
            bsFunctions.DataSource = dtFunctions;
            dgvListFunctions.DataSource = bsFunctions;

            dgvListFunctions.Columns["enabled"].DataPropertyName = "enabled";
            dgvListFunctions.Columns["dsfunction"].DataPropertyName = "dsfunction";
            dgvListFunctions.Columns["dtstart"].DataPropertyName = "dtstart";
            dgvListFunctions.Columns["dtstop"].DataPropertyName = "dtstop";
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busRole.save(addMode);
                //
                if (addMode)
                    busRole.id = Convert.ToInt32(busRole.getLastInsertId());
                else
                {
                    DataRowView CurrentRow = (DataRowView)bs.Current;
                    busRole.id = Convert.ToInt32(CurrentRow["id"]);
                }

                bsFunctions.EndEdit();
                if (dtFunctions != null)
                    busRole.saveRolesFunctions(busRole.id, dtFunctions);
                //
                dtFunctions.AcceptChanges();
                dgvList.Refresh();
                dgvListFunctions.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busRole.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
