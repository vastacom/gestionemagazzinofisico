﻿namespace WarMan.View
{
    partial class FrmCmDivisions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dSWarManBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSWarMan = new WarMan.DataAccess.DSWarMan();
            this.cmcompaniesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmcompaniesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cmcompaniescmbusinesspartnersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmcompaniesBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cmcompaniesBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCompany = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSWarManBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSWarMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniescmbusinesspartnersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource3)).BeginInit();
            this.SuspendLayout();
            // 
            // dSWarManBindingSource
            // 
            this.dSWarManBindingSource.DataSource = this.dSWarMan;
            this.dSWarManBindingSource.Position = 0;
            // 
            // dSWarMan
            // 
            this.dSWarMan.DataSetName = "DSWarMan";
            this.dSWarMan.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cmcompaniesBindingSource
            // 
            this.cmcompaniesBindingSource.DataMember = "cm_companies";
            this.cmcompaniesBindingSource.DataSource = this.dSWarMan;
            // 
            // cmcompaniesBindingSource1
            // 
            this.cmcompaniesBindingSource1.DataMember = "cm_companies";
            this.cmcompaniesBindingSource1.DataSource = this.dSWarManBindingSource;
            // 
            // cmcompaniescmbusinesspartnersBindingSource
            // 
            this.cmcompaniescmbusinesspartnersBindingSource.DataMember = "cm_companies_cm_business_partners";
            this.cmcompaniescmbusinesspartnersBindingSource.DataSource = this.cmcompaniesBindingSource;
            // 
            // cmcompaniesBindingSource2
            // 
            this.cmcompaniesBindingSource2.DataMember = "cm_companies";
            this.cmcompaniesBindingSource2.DataSource = this.dSWarManBindingSource;

        private new void InitializeComponent()
        {
            this.cmbCompany = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCompany
            // 
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(580, 119);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(121, 21);
            this.cmbCompany.TabIndex = 12;
            this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);

            // 
            // label1
            // 
            this.label1.AutoSize = true;

            this.label1.Location = new System.Drawing.Point(436, 127);

            this.label1.Location = new System.Drawing.Point(437, 124);

            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Company";
            // 

            // cmcompaniesBindingSource3
            // 
            this.cmcompaniesBindingSource3.DataMember = "cm_companies";
            this.cmcompaniesBindingSource3.DataSource = this.dSWarMan;
            // 
            // cmbCompany
            // 
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(580, 127);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(121, 21);
            this.cmbCompany.TabIndex = 14;
            // 

            // FrmCmDivisions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(800, 450);

            this.Controls.Add(this.cmbCompany);
            this.Controls.Add(this.label1);
            this.Name = "FrmCmDivisions";
            this.Text = "Divisions";
            this.DoubleClick += new System.EventHandler(this.listBox1_SelectedIndexChanged);

            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCompany);
            this.Name = "FrmCmDivisions";
            this.Text = "Divisions";
            this.Load += new System.EventHandler(this.Divisions_Load);

            this.Controls.SetChildIndex(this.lblCode, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.txtDescription, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnInsert, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);

            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.cmbCompany, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSWarManBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSWarMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniescmbusinesspartnersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmcompaniesBindingSource3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource dSWarManBindingSource;
        private DataAccess.DSWarMan dSWarMan;
        private System.Windows.Forms.BindingSource cmcompaniesBindingSource;
        private System.Windows.Forms.BindingSource cmcompaniesBindingSource1;
        private System.Windows.Forms.BindingSource cmcompaniesBindingSource2;
        private System.Windows.Forms.BindingSource cmcompaniescmbusinesspartnersBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource cmcompaniesBindingSource3;
        private System.Windows.Forms.ComboBox cmbCompany;

            this.Controls.SetChildIndex(this.cmbCompany, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

    }
}
