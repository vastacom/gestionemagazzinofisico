﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using WarMan.Business;
using WarMan.DataAccess;

namespace WarMan.View
{
    public partial class FrmMain : Form
    {
        BusFunction busFunction;
        BusUser busUser;

        public FrmMain()
        {
            InitializeComponent();
            busFunction = new BusFunction();
            busUser = new BusUser();
        }
        //
        private void FrmMain_Load(object sender, EventArgs e)
        {
            //User.IsAuthenticated = true;
            // Autenticazione
            if (!User.IsAuthenticated)
            {
                FrmLogin frmLogin = new FrmLogin();
                frmLogin.ShowDialog();
            }

            busUser.idRole = Convert.ToInt32(User.ruolo);

            DSWarMan.sy_functionsDataTable dt = busFunction.getListByRole(busUser.idRole);
            String menuGroup = String.Empty;
            foreach (DSWarMan.sy_functionsRow row in dt.Rows)
            {
                if (row.var1 != menuGroup)
                    tvMainMenu.Nodes.Add(row.var1, row.var4);

                tvMainMenu.Nodes[row.var1].Nodes.Add(row.functionkey, row.dsfunction);
                menuGroup = row.var1;
            }
        }
        //
        private void tvMainMenu_DoubleClick(object sender, EventArgs e)
        {
            if (tvMainMenu.SelectedNode != null && tvMainMenu.SelectedNode.Nodes.Count == 0)
            {
                String frmName = "WarMan.View." + busFunction.getFormNameByFunctionKey(tvMainMenu.SelectedNode.Name);
                openForm(Type.GetType(frmName));
            }
        }
        //
        private void openForm(Type classForm)
        {
            try
            {
                if (classForm == null)
                    throw new Exception();

                Form f = Application.OpenForms[classForm.Name];
                if (f != null)
                    f.BringToFront();
                else
                {
                    f = (Form)Activator.CreateInstance(classForm);
                    f.MdiParent = this;
                    f.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Funzione non ancora implementata.", "Avviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        //
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //
    }
}
