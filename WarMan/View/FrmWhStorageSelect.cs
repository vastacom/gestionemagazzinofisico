﻿using System;
using System.Data;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhStorageSelect : Form
    {
        BusWarehouseMaster busWarehouseMaster;
        BusStorageOrder busStorageOrder;

        public FrmWhStorageSelect()
        {
            InitializeComponent();

            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Any>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbMagazzino.DataSource = dtCmbWarehouse;
            cmbMagazzino.ValueMember = "id";
            cmbMagazzino.DisplayMember = "dswarehousemaster";
            //
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                busStorageOrder = new BusStorageOrder();
                DataTable dt = busStorageOrder.search(
                    txtMateriale.Text,
                    Convert.ToInt32(cmbMagazzino.SelectedValue),
                    "INSERITO"
                    );
                dt.Merge(busStorageOrder.search(
                    txtMateriale.Text,
                    Convert.ToInt32(cmbMagazzino.SelectedValue),
                    "ESEGUITO PARZ."
                    ));

                dgvStorageOrder.AutoGenerateColumns = false;
                dgvStorageOrder.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnEsegui_Click(object sender, EventArgs e)
        {
            Int32 idStorageOrder = Convert.ToInt32(dgvStorageOrder.SelectedRows[0].Cells["nOrdine"].Value);

            FrmWhStorageExec f = new FrmWhStorageExec(idStorageOrder);
            f.ShowDialog(this);
        }
    }
}
