﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmCmCompanies : VastaControls.FrmMasterTable
    {
        BusCompany busCompany;

        public FrmCmCompanies()
        {
            InitializeComponent();
            
            busCompany = new BusCompany();

            dataPropCode = "cdcompany";
            dataPropDescription = "dscompany";
            
        }


        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busCompany.getList("deleted <> 1"));
                //txtSymbol.DataBindings.Add("Text", bs, dataPropSymbol);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busCompany.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()  
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busCompany.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmCmCompanies_Load(object sender, EventArgs e)
        {

        }
    }
}
