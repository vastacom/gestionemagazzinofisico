﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhQueryHandling : Form
    {
        BusWarehouseMaster busWarehouseMaster;
        BusHandling busHandling;

        public FrmWhQueryHandling()
        {
            InitializeComponent();
            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Select>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbWarehouse.DataSource = dtCmbWarehouse;
            cmbWarehouse.ValueMember = "id";
            cmbWarehouse.DisplayMember = "dswarehousemaster";

            // Valori iniziali date
            dtFrom.Value = DateTime.Now;
            dtTo.Value = DateTime.Now;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            busHandling = new BusHandling();
            busHandling.idWarehouseMaster = Convert.ToInt32(cmbWarehouse.SelectedValue);
            dgvHandling.AutoGenerateColumns = false;
            dgvHandling.DataSource = busHandling.search(txtCode.Text, dtFrom.Value, dtTo.Value);
        }

        private void btnStampaCartellino_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHandling.SelectedRows.Count <= 0)
                    throw new Exception("Selezionare un movimento.");

                busHandling = new BusHandling();
                busHandling.id = Convert.ToInt32(dgvHandling.SelectedRows[0].Cells["id"].Value);

                DataTable dt = busHandling.getVoucherById();

                FrmHandlingVoucher frmHandlingVoucher = new FrmHandlingVoucher(dt);
                frmHandlingVoucher.ShowDialog();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
