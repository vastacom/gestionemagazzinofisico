﻿using System;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace WarMan.View
{
    public partial class FrmLocationsBarCode : Form
    {
        private DataTable _dtLocations;

        public FrmLocationsBarCode()
        {
            InitializeComponent();
        }

        public FrmLocationsBarCode(DataTable dtLocations)
        {
            InitializeComponent();
            _dtLocations = dtLocations;
        }

        private void FrmLocationsBarCode_Load(object sender, EventArgs e)
        {
            ReportDataSource dsLocationBarcoder = new ReportDataSource();
            dsLocationBarcoder.Value = _dtLocations;
            dsLocationBarcoder.Name = "dsLocationMaster";

            this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(dsLocationBarcoder);

            this.reportViewer1.RefreshReport();
        }
    }
}
