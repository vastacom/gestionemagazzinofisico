﻿
namespace WarMan.View
{
    partial class FrmWhPickingOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.txtMateriale = new System.Windows.Forms.TextBox();
            this.lblMateriale = new System.Windows.Forms.Label();
            this.lblDescrizioneMateriale = new System.Windows.Forms.Label();
            this.lblQta = new System.Windows.Forms.Label();
            this.nQuantita = new System.Windows.Forms.NumericUpDown();
            this.cmbMagazzino = new System.Windows.Forms.ComboBox();
            this.lblMagazzino = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.btnSerchMaterial = new System.Windows.Forms.Button();
            this.lblUom = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nQuantita)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(31, 301);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(152, 63);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtMateriale
            // 
            this.txtMateriale.Location = new System.Drawing.Point(144, 80);
            this.txtMateriale.Name = "txtMateriale";
            this.txtMateriale.Size = new System.Drawing.Size(135, 20);
            this.txtMateriale.TabIndex = 5;
            this.txtMateriale.TextChanged += new System.EventHandler(this.txtMateriale_TextChanged);
            this.txtMateriale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMateriale_KeyPress);
            // 
            // lblMateriale
            // 
            this.lblMateriale.AutoSize = true;
            this.lblMateriale.Location = new System.Drawing.Point(28, 83);
            this.lblMateriale.Name = "lblMateriale";
            this.lblMateriale.Size = new System.Drawing.Size(50, 13);
            this.lblMateriale.TabIndex = 4;
            this.lblMateriale.Text = "Materiale";
            // 
            // lblDescrizioneMateriale
            // 
            this.lblDescrizioneMateriale.AutoSize = true;
            this.lblDescrizioneMateriale.Location = new System.Drawing.Point(335, 85);
            this.lblDescrizioneMateriale.Name = "lblDescrizioneMateriale";
            this.lblDescrizioneMateriale.Size = new System.Drawing.Size(0, 13);
            this.lblDescrizioneMateriale.TabIndex = 7;
            // 
            // lblQta
            // 
            this.lblQta.AutoSize = true;
            this.lblQta.Location = new System.Drawing.Point(28, 127);
            this.lblQta.Name = "lblQta";
            this.lblQta.Size = new System.Drawing.Size(47, 13);
            this.lblQta.TabIndex = 8;
            this.lblQta.Text = "Quantità";
            // 
            // nQuantita
            // 
            this.nQuantita.Location = new System.Drawing.Point(144, 125);
            this.nQuantita.Name = "nQuantita";
            this.nQuantita.Size = new System.Drawing.Size(120, 20);
            this.nQuantita.TabIndex = 9;
            // 
            // cmbMagazzino
            // 
            this.cmbMagazzino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMagazzino.FormattingEnabled = true;
            this.cmbMagazzino.Location = new System.Drawing.Point(144, 173);
            this.cmbMagazzino.Name = "cmbMagazzino";
            this.cmbMagazzino.Size = new System.Drawing.Size(121, 21);
            this.cmbMagazzino.TabIndex = 11;
            // 
            // lblMagazzino
            // 
            this.lblMagazzino.AutoSize = true;
            this.lblMagazzino.Location = new System.Drawing.Point(28, 176);
            this.lblMagazzino.Name = "lblMagazzino";
            this.lblMagazzino.Size = new System.Drawing.Size(58, 13);
            this.lblMagazzino.TabIndex = 10;
            this.lblMagazzino.Text = "Magazzino";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(144, 31);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.ReadOnly = true;
            this.txtNumero.Size = new System.Drawing.Size(135, 20);
            this.txtNumero.TabIndex = 13;
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(28, 34);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(66, 13);
            this.lblNumero.TabIndex = 12;
            this.lblNumero.Text = "Num. Ordine";
            // 
            // btnSerchMaterial
            // 
            this.btnSerchMaterial.Location = new System.Drawing.Point(292, 77);
            this.btnSerchMaterial.Name = "btnSerchMaterial";
            this.btnSerchMaterial.Size = new System.Drawing.Size(26, 24);
            this.btnSerchMaterial.TabIndex = 14;
            this.btnSerchMaterial.Text = "...";
            this.btnSerchMaterial.UseVisualStyleBackColor = true;
            this.btnSerchMaterial.Click += new System.EventHandler(this.btnSerchMaterial_Click);
            // 
            // lblUom
            // 
            this.lblUom.AutoSize = true;
            this.lblUom.Location = new System.Drawing.Point(281, 127);
            this.lblUom.Name = "lblUom";
            this.lblUom.Size = new System.Drawing.Size(0, 13);
            this.lblUom.TabIndex = 15;
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(458, 30);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(109, 20);
            this.txtState.TabIndex = 17;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(357, 34);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(32, 13);
            this.lblState.TabIndex = 16;
            this.lblState.Text = "Stato";
            // 
            // FrmWhPickingOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 383);
            this.Controls.Add(this.txtState);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblUom);
            this.Controls.Add(this.btnSerchMaterial);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.cmbMagazzino);
            this.Controls.Add(this.lblMagazzino);
            this.Controls.Add(this.nQuantita);
            this.Controls.Add(this.lblQta);
            this.Controls.Add(this.lblDescrizioneMateriale);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtMateriale);
            this.Controls.Add(this.lblMateriale);
            this.Name = "FrmWhPickingOrders";
            this.Text = "Creazione Ordine di Prelievo";
            ((System.ComponentModel.ISupportInitialize)(this.nQuantita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtMateriale;
        private System.Windows.Forms.Label lblMateriale;
        private System.Windows.Forms.Label lblDescrizioneMateriale;
        private System.Windows.Forms.Label lblQta;
        private System.Windows.Forms.NumericUpDown nQuantita;
        private System.Windows.Forms.ComboBox cmbMagazzino;
        private System.Windows.Forms.Label lblMagazzino;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Button btnSerchMaterial;
        private System.Windows.Forms.Label lblUom;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label lblState;
    }
}