namespace WarMan.View
{
    partial class FrmSyUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblRoles = new System.Windows.Forms.Label();
            this.dgvListRoles = new System.Windows.Forms.DataGridView();
            this.enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dsrole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblValidFrom = new System.Windows.Forms.Label();
            this.dtDateStart = new System.Windows.Forms.DateTimePicker();
            this.dtDateStop = new System.Windows.Forms.DateTimePicker();
            this.lblValidTo = new System.Windows.Forms.Label();
            this.lblGroups = new System.Windows.Forms.Label();
            this.dgvListGroups = new System.Windows.Forms.DataGridView();
            this.enabledgroup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dsgroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstartgroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtstopgroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListGroups)).BeginInit();
            this.SuspendLayout();
            //
            // dgvList
            //
            this.dgvList.Size = new System.Drawing.Size(375, 556);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(573, 77);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(573, 36);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(450, 84);
            // 
            // lblCode
            // 
            this.lblCode.Location = new System.Drawing.Point(450, 44);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(573, 513);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(439, 513);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(707, 513);
            // 
            // lblName
            // 
            this.lblName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(450, 123);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 12;
            this.lblName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(573, 116);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 13;
            // 
            // txtSurname
            // 
            this.txtSurname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSurname.Location = new System.Drawing.Point(573, 155);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(100, 20);
            this.txtSurname.TabIndex = 15;
            // 
            // lblSurname
            // 
            this.lblSurname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(450, 162);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(49, 13);
            this.lblSurname.TabIndex = 14;
            this.lblSurname.Text = "Surname";
            // 
            // lblRoles
            // 
            this.lblRoles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRoles.AutoSize = true;
            this.lblRoles.Location = new System.Drawing.Point(449, 267);
            this.lblRoles.Name = "lblRoles";
            this.lblRoles.Size = new System.Drawing.Size(80, 13);
            this.lblRoles.TabIndex = 19;
            this.lblRoles.Text = "Ruoli Assegnati";
            // 
            // dgvListRoles
            // 
            this.dgvListRoles.AllowUserToAddRows = false;
            this.dgvListRoles.AllowUserToDeleteRows = false;
            this.dgvListRoles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvListRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListRoles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.enabled,
            this.dsrole,
            this.dtstart,
            this.dtstop});
            this.dgvListRoles.Location = new System.Drawing.Point(447, 283);
            this.dgvListRoles.Name = "dgvListRoles";
            this.dgvListRoles.Size = new System.Drawing.Size(383, 100);
            this.dgvListRoles.TabIndex = 18;
            // 
            // enabled
            // 
            this.enabled.HeaderText = "Enabled";
            this.enabled.Name = "enabled";
            this.enabled.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.enabled.Width = 50;
            // 
            // dsrole
            // 
            this.dsrole.HeaderText = "Role";
            this.dsrole.Name = "dsrole";
            this.dsrole.ReadOnly = true;
            this.dsrole.Width = 120;
            // 
            // dtstart
            // 
            this.dtstart.HeaderText = "Date Start";
            this.dtstart.Name = "dtstart";
            this.dtstart.Width = 80;
            // 
            // dtstop
            // 
            this.dtstop.HeaderText = "Date Stop";
            this.dtstop.Name = "dtstop";
            this.dtstop.Width = 80;
            // 
            // lblPassword
            // 
            this.lblPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(450, 199);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 20;
            this.lblPassword.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(573, 196);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 21;
            // 
            // lblValidFrom
            // 
            this.lblValidFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValidFrom.AutoSize = true;
            this.lblValidFrom.Location = new System.Drawing.Point(450, 237);
            this.lblValidFrom.Name = "lblValidFrom";
            this.lblValidFrom.Size = new System.Drawing.Size(56, 13);
            this.lblValidFrom.TabIndex = 22;
            this.lblValidFrom.Text = "Valid From";
            // 
            // dtDateStart
            // 
            this.dtDateStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtDateStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateStart.Location = new System.Drawing.Point(573, 231);
            this.dtDateStart.Name = "dtDateStart";
            this.dtDateStart.Size = new System.Drawing.Size(100, 20);
            this.dtDateStart.TabIndex = 24;
            // 
            // dtDateStop
            // 
            this.dtDateStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtDateStop.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateStop.Location = new System.Drawing.Point(717, 231);
            this.dtDateStop.Name = "dtDateStop";
            this.dtDateStop.Size = new System.Drawing.Size(100, 20);
            this.dtDateStop.TabIndex = 26;
            // 
            // lblValidTo
            // 
            this.lblValidTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValidTo.AutoSize = true;
            this.lblValidTo.Location = new System.Drawing.Point(691, 237);
            this.lblValidTo.Name = "lblValidTo";
            this.lblValidTo.Size = new System.Drawing.Size(20, 13);
            this.lblValidTo.TabIndex = 25;
            this.lblValidTo.Text = "To";
            // 
            // lblGroups
            // 
            this.lblGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroups.AutoSize = true;
            this.lblGroups.Location = new System.Drawing.Point(446, 391);
            this.lblGroups.Name = "lblGroups";
            this.lblGroups.Size = new System.Drawing.Size(83, 13);
            this.lblGroups.TabIndex = 28;
            this.lblGroups.Text = "Gruppi Associati";
            // 
            // dgvListGroups
            // 
            this.dgvListGroups.AllowUserToAddRows = false;
            this.dgvListGroups.AllowUserToDeleteRows = false;
            this.dgvListGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvListGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListGroups.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.enabledgroup,
            this.dsgroup,
            this.dtstartgroup,
            this.dtstopgroup});
            this.dgvListGroups.Location = new System.Drawing.Point(446, 407);
            this.dgvListGroups.Name = "dgvListGroups";
            this.dgvListGroups.Size = new System.Drawing.Size(383, 98);
            this.dgvListGroups.TabIndex = 27;
            // 
            // enabledgroup
            // 
            this.enabledgroup.HeaderText = "Enabled";
            this.enabledgroup.Name = "enabledgroup";
            this.enabledgroup.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.enabledgroup.Width = 50;
            // 
            // dsgroup
            // 
            this.dsgroup.HeaderText = "Group";
            this.dsgroup.Name = "dsgroup";
            this.dsgroup.ReadOnly = true;
            this.dsgroup.Width = 120;
            // 
            // dtstartgroup
            // 
            this.dtstartgroup.HeaderText = "Date Start";
            this.dtstartgroup.Name = "dtstartgroup";
            this.dtstartgroup.Width = 80;
            // 
            // dtstopgroup
            // 
            this.dtstopgroup.HeaderText = "Date Stop";
            this.dtstopgroup.Name = "dtstopgroup";
            this.dtstopgroup.Width = 80;
            // 
            // FrmSyUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 585);
            this.Controls.Add(this.lblGroups);
            this.Controls.Add(this.dgvListGroups);
            this.Controls.Add(this.dtDateStop);
            this.Controls.Add(this.lblValidTo);
            this.Controls.Add(this.dtDateStart);
            this.Controls.Add(this.lblValidFrom);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblRoles);
            this.Controls.Add(this.dgvListRoles);
            this.Controls.Add(this.txtSurname);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.Name = "FrmSyUsers";
            this.Text = "Utenti";
            this.Controls.SetChildIndex(this.lblName, 0);
            this.Controls.SetChildIndex(this.txtName, 0);
            this.Controls.SetChildIndex(this.lblSurname, 0);
            this.Controls.SetChildIndex(this.txtSurname, 0);
            this.Controls.SetChildIndex(this.dgvListRoles, 0);
            this.Controls.SetChildIndex(this.lblRoles, 0);
            this.Controls.SetChildIndex(this.lblPassword, 0);
            this.Controls.SetChildIndex(this.lblCode, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.txtDescription, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnInsert, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.txtPassword, 0);
            this.Controls.SetChildIndex(this.lblValidFrom, 0);
            this.Controls.SetChildIndex(this.dtDateStart, 0);
            this.Controls.SetChildIndex(this.lblValidTo, 0);
            this.Controls.SetChildIndex(this.dtDateStop, 0);
            this.Controls.SetChildIndex(this.dgvListGroups, 0);
            this.Controls.SetChildIndex(this.lblGroups, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListGroups)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblRoles;
        private System.Windows.Forms.DataGridView dgvListRoles;
        private System.Windows.Forms.DataGridViewCheckBoxColumn enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsrole;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstart;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstop;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblValidFrom;
        private System.Windows.Forms.DateTimePicker dtDateStart;
        private System.Windows.Forms.DateTimePicker dtDateStop;
        private System.Windows.Forms.Label lblValidTo;
        private System.Windows.Forms.Label lblGroups;
        private System.Windows.Forms.DataGridView dgvListGroups;
        private System.Windows.Forms.DataGridViewCheckBoxColumn enabledgroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsgroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstartgroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtstopgroup;
    }
}