﻿
namespace WarMan.View
{
    partial class FrmWhQueryStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.dgvStock = new System.Windows.Forms.DataGridView();
            this.cdwarehousemaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdlocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdmaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsmaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datelastmovement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(147, 24);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(121, 21);
            this.cmbWarehouse.TabIndex = 17;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(10, 26);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(58, 13);
            this.lblWarehouse.TabIndex = 16;
            this.lblWarehouse.Text = "Magazzino";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(484, 11);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 50);
            this.btnSearch.TabIndex = 18;
            this.btnSearch.Text = "SEARCH";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(147, 56);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(163, 20);
            this.txtCode.TabIndex = 20;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(10, 58);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(86, 13);
            this.lblCode.TabIndex = 19;
            this.lblCode.Text = "Codice Materiale";
            // 
            // dgvStock
            // 
            this.dgvStock.AllowUserToAddRows = false;
            this.dgvStock.AllowUserToDeleteRows = false;
            this.dgvStock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStock.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cdwarehousemaster,
            this.cdlocation,
            this.cdmaterial,
            this.dsmaterial,
            this.quantity,
            this.datelastmovement});
            this.dgvStock.Location = new System.Drawing.Point(9, 106);
            this.dgvStock.Margin = new System.Windows.Forms.Padding(2);
            this.dgvStock.Name = "dgvStock";
            this.dgvStock.ReadOnly = true;
            this.dgvStock.RowHeadersWidth = 51;
            this.dgvStock.RowTemplate.Height = 24;
            this.dgvStock.Size = new System.Drawing.Size(598, 306);
            this.dgvStock.TabIndex = 21;
            // 
            // cdwarehousemaster
            // 
            this.cdwarehousemaster.DataPropertyName = "cdwarehousemaster";
            this.cdwarehousemaster.HeaderText = "Magazzino";
            this.cdwarehousemaster.MinimumWidth = 6;
            this.cdwarehousemaster.Name = "cdwarehousemaster";
            this.cdwarehousemaster.ReadOnly = true;
            this.cdwarehousemaster.Width = 125;
            // 
            // cdlocation
            // 
            this.cdlocation.DataPropertyName = "cdlocation";
            this.cdlocation.HeaderText = "Ubicazione";
            this.cdlocation.MinimumWidth = 6;
            this.cdlocation.Name = "cdlocation";
            this.cdlocation.ReadOnly = true;
            this.cdlocation.Width = 150;
            // 
            // cdmaterial
            // 
            this.cdmaterial.DataPropertyName = "cdmaterial";
            this.cdmaterial.HeaderText = "Cod. Materiale";
            this.cdmaterial.MinimumWidth = 6;
            this.cdmaterial.Name = "cdmaterial";
            this.cdmaterial.ReadOnly = true;
            this.cdmaterial.Width = 150;
            // 
            // dsmaterial
            // 
            this.dsmaterial.DataPropertyName = "dsmaterial";
            this.dsmaterial.HeaderText = "Descr. Materiale";
            this.dsmaterial.MinimumWidth = 6;
            this.dsmaterial.Name = "dsmaterial";
            this.dsmaterial.ReadOnly = true;
            this.dsmaterial.Width = 250;
            // 
            // quantity
            // 
            this.quantity.DataPropertyName = "quantity";
            this.quantity.HeaderText = "Q.tà";
            this.quantity.MinimumWidth = 6;
            this.quantity.Name = "quantity";
            this.quantity.ReadOnly = true;
            this.quantity.Width = 80;
            // 
            // datelastmovement
            // 
            this.datelastmovement.DataPropertyName = "datelastmovement";
            this.datelastmovement.HeaderText = "Data Ult. Mov.";
            this.datelastmovement.MinimumWidth = 6;
            this.datelastmovement.Name = "datelastmovement";
            this.datelastmovement.ReadOnly = true;
            this.datelastmovement.Width = 125;
            // 
            // FrmWhQueryStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 421);
            this.Controls.Add(this.dgvStock);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.lblWarehouse);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmWhQueryStock";
            this.Text = "Inquiry Giacenze";
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.DataGridView dgvStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdwarehousemaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdlocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdmaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsmaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn datelastmovement;
    }
}