﻿using System;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace WarMan.View
{
    public partial class FrmHandlingVoucher : Form
    {
        private DataTable _dtHandling;

        public FrmHandlingVoucher()
        {
            InitializeComponent();
        }

        public FrmHandlingVoucher(DataTable dtHandling)
        {
            InitializeComponent();
            _dtHandling = dtHandling;
        }

        private void FrmHandlingVoucher_Load(object sender, EventArgs e)
        {
            ReportDataSource dsHandlingVoucher = new ReportDataSource();
            dsHandlingVoucher.Value = _dtHandling;
            dsHandlingVoucher.Name = "dsHandlingVoucher";

            this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(dsHandlingVoucher);

            this.reportViewer1.RefreshReport();
        }
    }
}
