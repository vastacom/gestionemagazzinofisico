﻿using System;
using System.Data;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhMaterialCreate : Form
    {
        BusMaterialType busMaterialType;
        BusUom busUom;
        BusMaterialMaster _busMaterialMaster;
        Boolean _addMode;
        String strTitle;

        public FrmWhMaterialCreate(BusMaterialMaster busMaterialMaster, Boolean addMode)
        {
            InitializeComponent();
                
            // Riempi le combo
            busMaterialType = new BusMaterialType();
            DataTable dtCmbTipo = busMaterialType.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbTipo.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dsmaterialtype"] = "<Select>";
            dtCmbTipo.Rows.InsertAt(rowEmpty, 0);
            // nella combo
            cmbMaterialType.DataSource = dtCmbTipo;
            cmbMaterialType.ValueMember = "id";
            cmbMaterialType.DisplayMember = "dsmaterialtype";

            busUom = new BusUom();
            DataTable dtCmbUom = busUom.getList();
            // Add Empty
            rowEmpty = dtCmbUom.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dsuom"] = "<Select>";
            dtCmbUom.Rows.InsertAt(rowEmpty, 0);
            // nella combo
            cmbUdm.DataSource = dtCmbUom;
            cmbUdm.ValueMember = "id";
            cmbUdm.DisplayMember = "dsuom";

            // Details
            _addMode = addMode;
            _busMaterialMaster = busMaterialMaster;

            // Set the title
            strTitle = this.Text;
            if (_addMode)
            {
                txtMaterialCode.Text = _busMaterialMaster.code;
                this.Text = strTitle + " - Insert";
            }
            else
            {
                _busMaterialMaster.getDetailsById();
                txtMaterialCode.Text = _busMaterialMaster.code;
                txtDescrizione.Text = _busMaterialMaster.description;
                cmbMaterialType.SelectedValue = _busMaterialMaster.idType;
                cmbUdm.SelectedValue = _busMaterialMaster.idUom;

                this.Text = strTitle + " - Update";
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMaterialCode.Text == String.Empty)
                    throw new Exception("Inserire un codice materiale.");
                if (txtDescrizione.Text == String.Empty)
                    throw new Exception("Inserire una descrizione per il materiale.");
                if (Convert.ToInt32(cmbMaterialType.SelectedValue) <= 0)
                    throw new Exception("Inserire un tipo materiale.");
                if (Convert.ToInt32(cmbUdm.SelectedValue) <= 0)
                    throw new Exception("Inserire una unità di misura.");

                _busMaterialMaster.code = txtMaterialCode.Text;
                _busMaterialMaster.description = txtDescrizione.Text;
                _busMaterialMaster.idType = Convert.ToInt32(cmbMaterialType.SelectedValue);
                _busMaterialMaster.idUom = Convert.ToInt32(cmbUdm.SelectedValue);

                _busMaterialMaster.save(_addMode);
                if (_busMaterialMaster.id > 0)
                {
                    _addMode = false;
                    this.Text = strTitle + " - Update";
                }

                MessageBox.Show("Salvataggio Effettuato.", "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Attenzione!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
