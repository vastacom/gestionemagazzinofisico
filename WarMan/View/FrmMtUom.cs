using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmMtUom : FrmMasterTable
    {
        BusUom busUom;
        protected String dataPropSymbol;

        public FrmMtUom()
        {
            InitializeComponent();
            busUom = new BusUom();

            dataPropCode = "cduom";
            dataPropDescription = "dsuom";
            dataPropSymbol = "uomsymbol";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busUom.getList());
                txtSymbol.DataBindings.Add("Text", bs, dataPropSymbol);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busUom.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busUom.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
