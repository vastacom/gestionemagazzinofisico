namespace WarMan.View
{
    partial class FrmSyFunctions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblKey = new System.Windows.Forms.Label();
            this.txtVar3 = new System.Windows.Forms.TextBox();
            this.lblVar3 = new System.Windows.Forms.Label();
            this.txtVar4 = new System.Windows.Forms.TextBox();
            this.lblVar4 = new System.Windows.Forms.Label();
            this.txtVar5 = new System.Windows.Forms.TextBox();
            this.lblVar5 = new System.Windows.Forms.Label();
            this.txtVar2 = new System.Windows.Forms.TextBox();
            this.lblVar2 = new System.Windows.Forms.Label();
            this.txtVar1 = new System.Windows.Forms.TextBox();
            this.lblVar1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(432, 80);
            // 
            // txtKey
            // 
            this.txtKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKey.Location = new System.Drawing.Point(580, 116);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(148, 20);
            this.txtKey.TabIndex = 15;
            // 
            // lblKey
            // 
            this.lblKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKey.AutoSize = true;
            this.lblKey.Location = new System.Drawing.Point(432, 119);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(25, 13);
            this.lblKey.TabIndex = 14;
            this.lblKey.Text = "Key";
            // 
            // txtVar3
            // 
            this.txtVar3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVar3.Location = new System.Drawing.Point(580, 233);
            this.txtVar3.Name = "txtVar3";
            this.txtVar3.Size = new System.Drawing.Size(148, 20);
            this.txtVar3.TabIndex = 17;
            // 
            // lblVar3
            // 
            this.lblVar3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVar3.AutoSize = true;
            this.lblVar3.Location = new System.Drawing.Point(432, 236);
            this.lblVar3.Name = "lblVar3";
            this.lblVar3.Size = new System.Drawing.Size(28, 13);
            this.lblVar3.TabIndex = 16;
            this.lblVar3.Text = "var3";
            // 
            // txtVar4
            // 
            this.txtVar4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVar4.Location = new System.Drawing.Point(580, 272);
            this.txtVar4.Name = "txtVar4";
            this.txtVar4.Size = new System.Drawing.Size(148, 20);
            this.txtVar4.TabIndex = 19;
            // 
            // lblVar4
            // 
            this.lblVar4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVar4.AutoSize = true;
            this.lblVar4.Location = new System.Drawing.Point(432, 275);
            this.lblVar4.Name = "lblVar4";
            this.lblVar4.Size = new System.Drawing.Size(28, 13);
            this.lblVar4.TabIndex = 18;
            this.lblVar4.Text = "var4";
            // 
            // txtVar5
            // 
            this.txtVar5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVar5.Location = new System.Drawing.Point(580, 311);
            this.txtVar5.Name = "txtVar5";
            this.txtVar5.Size = new System.Drawing.Size(148, 20);
            this.txtVar5.TabIndex = 21;
            // 
            // lblVar5
            // 
            this.lblVar5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVar5.AutoSize = true;
            this.lblVar5.Location = new System.Drawing.Point(432, 314);
            this.lblVar5.Name = "lblVar5";
            this.lblVar5.Size = new System.Drawing.Size(28, 13);
            this.lblVar5.TabIndex = 20;
            this.lblVar5.Text = "var5";
            // 
            // txtVar2
            // 
            this.txtVar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVar2.Location = new System.Drawing.Point(580, 194);
            this.txtVar2.Name = "txtVar2";
            this.txtVar2.Size = new System.Drawing.Size(148, 20);
            this.txtVar2.TabIndex = 23;
            // 
            // lblVar2
            // 
            this.lblVar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVar2.AutoSize = true;
            this.lblVar2.Location = new System.Drawing.Point(432, 197);
            this.lblVar2.Name = "lblVar2";
            this.lblVar2.Size = new System.Drawing.Size(28, 13);
            this.lblVar2.TabIndex = 22;
            this.lblVar2.Text = "var2";
            // 
            // txtVar1
            // 
            this.txtVar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVar1.Location = new System.Drawing.Point(580, 155);
            this.txtVar1.Name = "txtVar1";
            this.txtVar1.Size = new System.Drawing.Size(148, 20);
            this.txtVar1.TabIndex = 25;
            // 
            // lblVar1
            // 
            this.lblVar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVar1.AutoSize = true;
            this.lblVar1.Location = new System.Drawing.Point(432, 158);
            this.lblVar1.Name = "lblVar1";
            this.lblVar1.Size = new System.Drawing.Size(28, 13);
            this.lblVar1.TabIndex = 24;
            this.lblVar1.Text = "var1";
            // 
            // FrmSyFunctions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtVar1);
            this.Controls.Add(this.lblVar1);
            this.Controls.Add(this.txtVar2);
            this.Controls.Add(this.lblVar2);
            this.Controls.Add(this.txtVar5);
            this.Controls.Add(this.lblVar5);
            this.Controls.Add(this.txtVar4);
            this.Controls.Add(this.lblVar4);
            this.Controls.Add(this.txtVar3);
            this.Controls.Add(this.lblVar3);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.lblKey);
            this.Name = "FrmSyFunctions";
            this.Text = "Funzioni";
            this.Controls.SetChildIndex(this.lblCode, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.txtDescription, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnInsert, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.lblKey, 0);
            this.Controls.SetChildIndex(this.txtKey, 0);
            this.Controls.SetChildIndex(this.lblVar3, 0);
            this.Controls.SetChildIndex(this.txtVar3, 0);
            this.Controls.SetChildIndex(this.lblVar4, 0);
            this.Controls.SetChildIndex(this.txtVar4, 0);
            this.Controls.SetChildIndex(this.lblVar5, 0);
            this.Controls.SetChildIndex(this.txtVar5, 0);
            this.Controls.SetChildIndex(this.lblVar2, 0);
            this.Controls.SetChildIndex(this.txtVar2, 0);
            this.Controls.SetChildIndex(this.lblVar1, 0);
            this.Controls.SetChildIndex(this.txtVar1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.TextBox txtVar3;
        private System.Windows.Forms.Label lblVar3;
        private System.Windows.Forms.TextBox txtVar4;
        private System.Windows.Forms.Label lblVar4;
        private System.Windows.Forms.TextBox txtVar5;
        private System.Windows.Forms.Label lblVar5;
        private System.Windows.Forms.TextBox txtVar2;
        private System.Windows.Forms.Label lblVar2;
        private System.Windows.Forms.TextBox txtVar1;
        private System.Windows.Forms.Label lblVar1;
    }
}