using System;
using System.Data;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmSyUsers : FrmMasterTable
    {
        BusUser busUser;
        //
        DataTable dtRoles = new DataTable("dtRoles");
        BindingSource bsRoles = new BindingSource();
        //
        DataTable dtGroups = new DataTable("dtGroups");
        BindingSource bsGroups = new BindingSource();

        public FrmSyUsers()
        {
            InitializeComponent();
            busUser = new BusUser();

            dataPropCode = "username";
            dataPropDescription = "email";

            dgvList.Columns[0].HeaderText = "UserName";
            dgvList.Columns[1].HeaderText = "eMail";

            lblCode.Text = "UserName";
            lblDescription.Text = "eMail";

            // Campo aggiunto
            DataGridViewTextBoxColumn dgcActive = new DataGridViewTextBoxColumn();
            dgcActive.Name = "active";
            dgcActive.HeaderText = "Attivo";
            dgcActive.DataPropertyName = "active";
            dgcActive.ReadOnly = true;
            dgvList.Columns.Add(dgcActive);
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busUser.getList());
                txtName.DataBindings.Add("Text", bs, "name");
                txtSurname.DataBindings.Add("Text", bs, "surname");
                txtPassword.DataBindings.Add("Text", bs, "password");
                dtDateStart.DataBindings.Add("Text", bs, "dtstart");
                dtDateStop.DataBindings.Add("Text", bs, "dtstop");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
        protected override void dgvList_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if ((dtRoles.GetChanges() != null || dtGroups.GetChanges() != null) && dgvList.Focused)
            {
                DialogResult result = MessageBox.Show("I dati non salvati andranno persi.\nContinuare?", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
        //
        protected override void dgvList_SelectionChanged(object sender, EventArgs e)
        {
            if (addMode)
                return;
            //
            DataRowView CurrentRow = (DataRowView)bs.Current;
            busUser.id = Convert.ToInt32(CurrentRow.Row["id"]);

            // Roles Enabled
            dgvListRoles.AutoGenerateColumns = false;
            dtRoles = busUser.getRolesByUser();
            bsRoles.DataSource = dtRoles;
            dgvListRoles.DataSource = bsRoles;

            dgvListRoles.Columns["enabled"].DataPropertyName = "enabled";
            dgvListRoles.Columns["dsrole"].DataPropertyName = "dsrole";
            dgvListRoles.Columns["dtstart"].DataPropertyName = "dtstart";
            dgvListRoles.Columns["dtstop"].DataPropertyName = "dtstop";

            // Group Associated
            dgvListGroups.AutoGenerateColumns = false;
            dtGroups = busUser.getGroupsByUser();
            bsGroups.DataSource = dtGroups;
            dgvListGroups.DataSource = bsGroups;

            dgvListGroups.Columns["enabledgroup"].DataPropertyName = "enabled";
            dgvListGroups.Columns["dsgroup"].DataPropertyName = "dsgroup";
            dgvListGroups.Columns["dtstartgroup"].DataPropertyName = "dtstart";
            dgvListGroups.Columns["dtstopgroup"].DataPropertyName = "dtstop";
        }

        //
        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busUser.save(addMode);
                //
                if (addMode)
                    busUser.id = Convert.ToInt32(busUser.getLastInsertId());
                else
                {
                    DataRowView CurrentRow = (DataRowView)bs.Current;
                    busUser.id = Convert.ToInt32(CurrentRow["id"]);
                }

                bsRoles.EndEdit();
                if (dtRoles != null)
                    busUser.saveUsersRoles(busUser.id, dtRoles);
                dtRoles.AcceptChanges();
                //
                bsGroups.EndEdit();
                if (dtGroups != null)
                    busUser.saveUsersGroups(busUser.id, dtGroups);
                dtGroups.AcceptChanges();
                //
                dgvList.Refresh();
                dgvListRoles.Refresh();
                dgvListGroups.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busUser.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //
    }
}
