﻿namespace WarMan.View
{
    partial class FrmWhLocationMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbLocationType = new System.Windows.Forms.ComboBox();
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblLocationType = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.dgvLocation = new System.Windows.Forms.DataGridView();
            this.cdwarehousemaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdlocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dslocationtype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdroot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnBarcode = new System.Windows.Forms.Button();
            this.lblCodiceUbicazione = new System.Windows.Forms.Label();
            this.lblNumPiani = new System.Windows.Forms.Label();
            this.lblNumFile = new System.Windows.Forms.Label();
            this.btnGenerazione = new System.Windows.Forms.Button();
            this.txtCodiceUbicazione = new System.Windows.Forms.TextBox();
            this.numPiani = new System.Windows.Forms.NumericUpDown();
            this.numFile = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPiani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFile)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbLocationType
            // 
            this.cmbLocationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLocationType.FormattingEnabled = true;
            this.cmbLocationType.Location = new System.Drawing.Point(181, 68);
            this.cmbLocationType.Name = "cmbLocationType";
            this.cmbLocationType.Size = new System.Drawing.Size(121, 21);
            this.cmbLocationType.TabIndex = 16;
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(181, 28);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(121, 21);
            this.cmbWarehouse.TabIndex = 15;
            // 
            // lblLocationType
            // 
            this.lblLocationType.AutoSize = true;
            this.lblLocationType.Location = new System.Drawing.Point(33, 76);
            this.lblLocationType.Name = "lblLocationType";
            this.lblLocationType.Size = new System.Drawing.Size(84, 13);
            this.lblLocationType.TabIndex = 14;
            this.lblLocationType.Text = "Tipo Ubicazione";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(33, 36);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(58, 13);
            this.lblWarehouse.TabIndex = 13;
            this.lblWarehouse.Text = "Magazzino";
            // 
            // btnFilter
            // 
            this.btnFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFilter.Location = new System.Drawing.Point(36, 171);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(136, 57);
            this.btnFilter.TabIndex = 17;
            this.btnFilter.Text = "Trova";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // dgvLocation
            // 
            this.dgvLocation.AllowUserToAddRows = false;
            this.dgvLocation.AllowUserToDeleteRows = false;
            this.dgvLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cdwarehousemaster,
            this.cdlocation,
            this.dslocationtype,
            this.cdroot});
            this.dgvLocation.Location = new System.Drawing.Point(430, 12);
            this.dgvLocation.Name = "dgvLocation";
            this.dgvLocation.ReadOnly = true;
            this.dgvLocation.Size = new System.Drawing.Size(461, 532);
            this.dgvLocation.TabIndex = 18;
            // 
            // cdwarehousemaster
            // 
            this.cdwarehousemaster.DataPropertyName = "cdwarehousemaster";
            this.cdwarehousemaster.HeaderText = "Codice Magazzino";
            this.cdwarehousemaster.Name = "cdwarehousemaster";
            this.cdwarehousemaster.ReadOnly = true;
            // 
            // cdlocation
            // 
            this.cdlocation.DataPropertyName = "cdlocation";
            this.cdlocation.HeaderText = "Codice Ubicazione";
            this.cdlocation.Name = "cdlocation";
            this.cdlocation.ReadOnly = true;
            this.cdlocation.Width = 200;
            // 
            // dslocationtype
            // 
            this.dslocationtype.DataPropertyName = "dslocationtype";
            this.dslocationtype.FillWeight = 150F;
            this.dslocationtype.HeaderText = "Tipo Ubicazione";
            this.dslocationtype.Name = "dslocationtype";
            this.dslocationtype.ReadOnly = true;
            // 
            // cdroot
            // 
            this.cdroot.DataPropertyName = "cdroot";
            this.cdroot.HeaderText = "Codice Radice";
            this.cdroot.Name = "cdroot";
            this.cdroot.ReadOnly = true;
            // 
            // btnBarcode
            // 
            this.btnBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBarcode.Location = new System.Drawing.Point(430, 550);
            this.btnBarcode.Name = "btnBarcode";
            this.btnBarcode.Size = new System.Drawing.Size(240, 57);
            this.btnBarcode.TabIndex = 19;
            this.btnBarcode.Text = "Stampa Barcode";
            this.btnBarcode.UseVisualStyleBackColor = true;
            this.btnBarcode.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // lblCodiceUbicazione
            // 
            this.lblCodiceUbicazione.AutoSize = true;
            this.lblCodiceUbicazione.Location = new System.Drawing.Point(33, 114);
            this.lblCodiceUbicazione.Name = "lblCodiceUbicazione";
            this.lblCodiceUbicazione.Size = new System.Drawing.Size(96, 13);
            this.lblCodiceUbicazione.TabIndex = 20;
            this.lblCodiceUbicazione.Text = "Codice Ubicazione";
            // 
            // lblNumPiani
            // 
            this.lblNumPiani.AutoSize = true;
            this.lblNumPiani.Location = new System.Drawing.Point(33, 287);
            this.lblNumPiani.Name = "lblNumPiani";
            this.lblNumPiani.Size = new System.Drawing.Size(70, 13);
            this.lblNumPiani.TabIndex = 21;
            this.lblNumPiani.Text = "Numero Piani";
            // 
            // lblNumFile
            // 
            this.lblNumFile.AutoSize = true;
            this.lblNumFile.Location = new System.Drawing.Point(33, 335);
            this.lblNumFile.Name = "lblNumFile";
            this.lblNumFile.Size = new System.Drawing.Size(63, 13);
            this.lblNumFile.TabIndex = 22;
            this.lblNumFile.Text = "Numero File";
            // 
            // btnGenerazione
            // 
            this.btnGenerazione.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerazione.Location = new System.Drawing.Point(36, 396);
            this.btnGenerazione.Name = "btnGenerazione";
            this.btnGenerazione.Size = new System.Drawing.Size(245, 57);
            this.btnGenerazione.TabIndex = 23;
            this.btnGenerazione.Text = "Genera Ubicazioni";
            this.btnGenerazione.UseVisualStyleBackColor = true;
            this.btnGenerazione.Click += new System.EventHandler(this.btnGenerazione_Click);
            // 
            // txtCodiceUbicazione
            // 
            this.txtCodiceUbicazione.Location = new System.Drawing.Point(181, 111);
            this.txtCodiceUbicazione.Name = "txtCodiceUbicazione";
            this.txtCodiceUbicazione.Size = new System.Drawing.Size(100, 20);
            this.txtCodiceUbicazione.TabIndex = 24;
            // 
            // numPiani
            // 
            this.numPiani.Location = new System.Drawing.Point(181, 285);
            this.numPiani.Name = "numPiani";
            this.numPiani.Size = new System.Drawing.Size(100, 20);
            this.numPiani.TabIndex = 27;
            // 
            // numFile
            // 
            this.numFile.Location = new System.Drawing.Point(181, 333);
            this.numFile.Name = "numFile";
            this.numFile.Size = new System.Drawing.Size(100, 20);
            this.numFile.TabIndex = 28;
            // 
            // FrmWhLocationMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 611);
            this.Controls.Add(this.numFile);
            this.Controls.Add(this.numPiani);
            this.Controls.Add(this.txtCodiceUbicazione);
            this.Controls.Add(this.btnGenerazione);
            this.Controls.Add(this.lblNumFile);
            this.Controls.Add(this.lblNumPiani);
            this.Controls.Add(this.lblCodiceUbicazione);
            this.Controls.Add(this.btnBarcode);
            this.Controls.Add(this.dgvLocation);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.cmbLocationType);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.lblLocationType);
            this.Controls.Add(this.lblWarehouse);
            this.Name = "FrmWhLocationMaster";
            this.Text = "Anagrafica Ubicazioni";
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPiani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbLocationType;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Label lblLocationType;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView dgvLocation;
        private System.Windows.Forms.Button btnBarcode;
        private System.Windows.Forms.Label lblCodiceUbicazione;
        private System.Windows.Forms.Label lblNumPiani;
        private System.Windows.Forms.Label lblNumFile;
        private System.Windows.Forms.Button btnGenerazione;
        private System.Windows.Forms.TextBox txtCodiceUbicazione;
        private System.Windows.Forms.NumericUpDown numPiani;
        private System.Windows.Forms.NumericUpDown numFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdwarehousemaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdlocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dslocationtype;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdroot;
    }
}