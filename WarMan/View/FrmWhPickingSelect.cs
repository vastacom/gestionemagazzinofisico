﻿using System;
using System.Data;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhPickingSelect : Form
    {
        BusWarehouseMaster busWarehouseMaster;
        BusPickingOrder busPickingOrder;

        public FrmWhPickingSelect()
        {
            InitializeComponent();

            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Any>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbMagazzino.DataSource = dtCmbWarehouse;
            cmbMagazzino.ValueMember = "id";
            cmbMagazzino.DisplayMember = "dswarehousemaster";
            //
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                busPickingOrder = new BusPickingOrder();
                DataTable dt = busPickingOrder.search(
                    txtMateriale.Text,
                    Convert.ToInt32(cmbMagazzino.SelectedValue),
                    "INSERITO"
                    );
                dt.Merge(busPickingOrder.search(
                    txtMateriale.Text,
                    Convert.ToInt32(cmbMagazzino.SelectedValue),
                    "ESEGUITO PARZ."
                    ));

                dgvPickingOrder.AutoGenerateColumns = false;
                dgvPickingOrder.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnEsegui_Click(object sender, EventArgs e)
        {
            Int32 idPickingOrder = Convert.ToInt32(dgvPickingOrder.SelectedRows[0].Cells["nOrdine"].Value);

            FrmWhPickingExec f = new FrmWhPickingExec(idPickingOrder);
            f.ShowDialog(this);
        }
    }
}
