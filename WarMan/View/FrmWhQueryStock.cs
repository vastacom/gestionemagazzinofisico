﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmWhQueryStock : Form
    {
        BusWarehouseMaster busWarehouseMaster;
        BusStock busStock;

        public FrmWhQueryStock()
        {
            InitializeComponent();
            // Riempi le combo
            busWarehouseMaster = new BusWarehouseMaster();
            DataTable dtCmbWarehouse = busWarehouseMaster.getList();
            // Add Empty
            DataRow rowEmpty = dtCmbWarehouse.NewRow();
            rowEmpty["id"] = "-1";
            rowEmpty["dswarehousemaster"] = "<Select>";
            dtCmbWarehouse.Rows.InsertAt(rowEmpty, 0);

            cmbWarehouse.DataSource = dtCmbWarehouse;
            cmbWarehouse.ValueMember = "id";
            cmbWarehouse.DisplayMember = "dswarehousemaster";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            busStock = new BusStock();
            busStock.idWarehouseMaster = Convert.ToInt32(cmbWarehouse.SelectedValue);
            dgvStock.AutoGenerateColumns = false;
            dgvStock.DataSource = busStock.search(txtCode.Text);
        }
    }
}
