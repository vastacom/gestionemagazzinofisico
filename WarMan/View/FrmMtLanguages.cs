using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VastaControls;
using WarMan.Business;


namespace WarMan.View
{
    public partial class FrmMtLanguages : FrmMasterTable
    {
        BusLanguage busLanguage;

        public FrmMtLanguages()
        {
            InitializeComponent();
            busLanguage = new BusLanguage();

            dataPropCode = "cdlanguage";
            dataPropDescription = "dslanguage";
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busLanguage.getList());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busLanguage.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busLanguage.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}
