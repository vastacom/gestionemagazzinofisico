﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WarMan.Business;

namespace WarMan.View
{
    public partial class FrmCmDivisions : VastaControls.FrmMasterTable
    {

        protected String dataPropIdCompany;

        BusDivision busDivision;
        BusCompany busCompany;
        public FrmCmDivisions()
        {
            InitializeComponent();
            busDivision = new BusDivision();

            // Riempi le combo
            busCompany = new BusCompany();
            cmbCompany.DataSource = busCompany.getList("deleted <> 1");
            cmbCompany.ValueMember = "id";
            cmbCompany.DisplayMember = "dscompany";

            dataPropCode = "cddivision";
            dataPropDescription = "dsdivision";
            dataPropIdCompany = "idcompany";
        }


        protected override void OnLoad(EventArgs e)
        {
            try
            {
                FormLoad(busDivision.getList("deleted <> 1"));
                cmbCompany.DataBindings.Add("SelectedValue", bs, "id");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Save()
        {
            try
            {
                bs.EndEdit();
                busDivision.save(addMode);
                dgvList.Refresh();
                addMode = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void Insert()
        {
            bs.AddNew();
            DataRowView CurrentRow = (DataRowView)bs.Current;
            CurrentRow.Row["deleted"] = 0;
            addMode = true;
        }

        protected override void Delete()  
        {
            try
            {
                DataRowView CurrentRow = (DataRowView)bs.Current;
                CurrentRow.Row["deleted"] = 1;
                bs.EndEdit();
                busDivision.save(addMode);
                bs.RemoveCurrent();
                dgvList.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void loadList(object sender, EventArgs e)
        {

        }


        private void Divisions_Load(object sender, EventArgs e)
        {

        }

        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
