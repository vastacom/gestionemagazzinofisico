﻿
namespace WarMan.View
{
    partial class FrmWhQueryHandling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.dgvHandling = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datehandling = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdmaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsmaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typehandling = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdwarehousemaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dswarehousemaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdlocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.lblDtFrom = new System.Windows.Forms.Label();
            this.lblDtTo = new System.Windows.Forms.Label();
            this.btnStampaCartellino = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHandling)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(147, 24);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(121, 21);
            this.cmbWarehouse.TabIndex = 17;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(10, 26);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(58, 13);
            this.lblWarehouse.TabIndex = 16;
            this.lblWarehouse.Text = "Magazzino";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(594, 11);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 50);
            this.btnSearch.TabIndex = 18;
            this.btnSearch.Text = "SEARCH";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(147, 56);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(163, 20);
            this.txtCode.TabIndex = 20;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(10, 58);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(86, 13);
            this.lblCode.TabIndex = 19;
            this.lblCode.Text = "Codice Materiale";
            // 
            // dgvHandling
            // 
            this.dgvHandling.AllowUserToAddRows = false;
            this.dgvHandling.AllowUserToDeleteRows = false;
            this.dgvHandling.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvHandling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHandling.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.datehandling,
            this.cdmaterial,
            this.dsmaterial,
            this.typehandling,
            this.cdwarehousemaster,
            this.dswarehousemaster,
            this.cdlocation,
            this.quantity});
            this.dgvHandling.Location = new System.Drawing.Point(12, 112);
            this.dgvHandling.Margin = new System.Windows.Forms.Padding(2);
            this.dgvHandling.MultiSelect = false;
            this.dgvHandling.Name = "dgvHandling";
            this.dgvHandling.ReadOnly = true;
            this.dgvHandling.RowHeadersWidth = 51;
            this.dgvHandling.RowTemplate.Height = 24;
            this.dgvHandling.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHandling.Size = new System.Drawing.Size(709, 323);
            this.dgvHandling.TabIndex = 21;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // datehandling
            // 
            this.datehandling.DataPropertyName = "datehandling";
            this.datehandling.HeaderText = "Data Mov.";
            this.datehandling.MinimumWidth = 6;
            this.datehandling.Name = "datehandling";
            this.datehandling.ReadOnly = true;
            this.datehandling.Width = 125;
            // 
            // cdmaterial
            // 
            this.cdmaterial.DataPropertyName = "cdmaterial";
            this.cdmaterial.HeaderText = "Cod. Materiale";
            this.cdmaterial.MinimumWidth = 6;
            this.cdmaterial.Name = "cdmaterial";
            this.cdmaterial.ReadOnly = true;
            this.cdmaterial.Width = 150;
            // 
            // dsmaterial
            // 
            this.dsmaterial.DataPropertyName = "dsmaterial";
            this.dsmaterial.HeaderText = "Descr. Materiale";
            this.dsmaterial.MinimumWidth = 6;
            this.dsmaterial.Name = "dsmaterial";
            this.dsmaterial.ReadOnly = true;
            this.dsmaterial.Width = 250;
            // 
            // typehandling
            // 
            this.typehandling.DataPropertyName = "typehandling";
            this.typehandling.HeaderText = "Tipo Mov.";
            this.typehandling.MinimumWidth = 6;
            this.typehandling.Name = "typehandling";
            this.typehandling.ReadOnly = true;
            this.typehandling.Width = 125;
            // 
            // cdwarehousemaster
            // 
            this.cdwarehousemaster.DataPropertyName = "cdwarehousemaster";
            this.cdwarehousemaster.HeaderText = "Cod. Mag.";
            this.cdwarehousemaster.MinimumWidth = 6;
            this.cdwarehousemaster.Name = "cdwarehousemaster";
            this.cdwarehousemaster.ReadOnly = true;
            this.cdwarehousemaster.Width = 125;
            // 
            // dswarehousemaster
            // 
            this.dswarehousemaster.DataPropertyName = "dswarehousemaster";
            this.dswarehousemaster.HeaderText = "Descr. Magazzino";
            this.dswarehousemaster.MinimumWidth = 6;
            this.dswarehousemaster.Name = "dswarehousemaster";
            this.dswarehousemaster.ReadOnly = true;
            this.dswarehousemaster.Width = 180;
            // 
            // cdlocation
            // 
            this.cdlocation.DataPropertyName = "cdlocation";
            this.cdlocation.HeaderText = "Ubicazione";
            this.cdlocation.MinimumWidth = 6;
            this.cdlocation.Name = "cdlocation";
            this.cdlocation.ReadOnly = true;
            this.cdlocation.Width = 150;
            // 
            // quantity
            // 
            this.quantity.DataPropertyName = "quantity";
            this.quantity.HeaderText = "Q.tà";
            this.quantity.MinimumWidth = 6;
            this.quantity.Name = "quantity";
            this.quantity.ReadOnly = true;
            this.quantity.Width = 80;
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(453, 24);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(92, 20);
            this.dtFrom.TabIndex = 22;
            this.dtFrom.Value = new System.DateTime(2021, 7, 27, 14, 3, 19, 0);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(453, 56);
            this.dtTo.Margin = new System.Windows.Forms.Padding(2);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(92, 20);
            this.dtTo.TabIndex = 23;
            this.dtTo.Value = new System.DateTime(2021, 7, 27, 14, 3, 30, 0);
            // 
            // lblDtFrom
            // 
            this.lblDtFrom.AutoSize = true;
            this.lblDtFrom.Location = new System.Drawing.Point(386, 28);
            this.lblDtFrom.Name = "lblDtFrom";
            this.lblDtFrom.Size = new System.Drawing.Size(48, 13);
            this.lblDtFrom.TabIndex = 24;
            this.lblDtFrom.Text = "Data da:";
            // 
            // lblDtTo
            // 
            this.lblDtTo.AutoSize = true;
            this.lblDtTo.Location = new System.Drawing.Point(386, 60);
            this.lblDtTo.Name = "lblDtTo";
            this.lblDtTo.Size = new System.Drawing.Size(42, 13);
            this.lblDtTo.TabIndex = 25;
            this.lblDtTo.Text = "Data a:";
            // 
            // btnStampaCartellino
            // 
            this.btnStampaCartellino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStampaCartellino.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStampaCartellino.Location = new System.Drawing.Point(580, 441);
            this.btnStampaCartellino.Name = "btnStampaCartellino";
            this.btnStampaCartellino.Size = new System.Drawing.Size(138, 61);
            this.btnStampaCartellino.TabIndex = 26;
            this.btnStampaCartellino.Text = "RISTAMPA CARTELLINO";
            this.btnStampaCartellino.UseVisualStyleBackColor = true;
            this.btnStampaCartellino.Click += new System.EventHandler(this.btnStampaCartellino_Click);
            // 
            // FrmWhQueryHandling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 513);
            this.Controls.Add(this.btnStampaCartellino);
            this.Controls.Add(this.lblDtTo);
            this.Controls.Add(this.lblDtFrom);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.dtFrom);
            this.Controls.Add(this.dgvHandling);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.lblWarehouse);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmWhQueryHandling";
            this.Text = "Inquiry Movimenti";
            ((System.ComponentModel.ISupportInitialize)(this.dgvHandling)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.DataGridView dgvHandling;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label lblDtFrom;
        private System.Windows.Forms.Label lblDtTo;
        private System.Windows.Forms.Button btnStampaCartellino;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn datehandling;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdmaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsmaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn typehandling;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdwarehousemaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn dswarehousemaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdlocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
    }
}